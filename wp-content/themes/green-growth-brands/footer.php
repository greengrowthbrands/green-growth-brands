		<footer class="footer">
			<div class="container">
				<div class="footer__inner">
					<a href="<?php echo home_url(); ?>" class="logo">
						<img src="<?php echo get_template_directory_uri() ?>/resources/images/logo.svg" alt="">
					</a>
					
					<?php
					if ( has_nav_menu( 'footer-menu' ) ) {
						wp_nav_menu( array(
							'theme_location' => 'footer-menu',
							'container' => 'nav',
							'container_class' => 'footer__nav'
						) );
					}
					?>
					
					<?php if ( get_field( 'show_investors_button', 'option' ) ) : ?>
						<a href="<?php echo get_the_permalink( crb_get_page_ID_by_template( 'templates/for-investors.php' ) ) ?>" class="btn btn--transparent"><?php _e( 'investors', 'crb' ); ?></a>
					<?php endif; ?>
				</div><!-- /.footer__inner -->
				
				<div class="footer__bar">
					<?php if ( $copyright = get_field( 'footer_copyright', 'option' ) ) : ?>
						<p class="footer__copyright"><?php echo esc_html( do_shortcode( $copyright ) ); ?></p><!-- /.footer__copyright -->
					<?php endif; ?>

					<?php
					$socials = [
						'facebook' => 'fa-facebook-f',
						'twitter' => 'fa-twitter',
						'linkedin' => 'fa-linkedin-in'
					];
					?>
					<ul class="footer__socials">
						<?php foreach ( $socials as $social => $class ) : $field_name = $social . '_link'; ?>
							<?php if ( $link = get_field( $field_name, 'option' ) ) : ?>
								<li>
									<a href="<?php echo esc_url( $link ); ?>">
										<i class="fab <?php echo $class ?>"></i>
									</a>
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					</ul><!-- /.footer__socials -->

					<ul class="list-links">
						<?php if ( $privacy_policy_link = get_field( 'footer_privacy_policy_link', 'option' ) ) : ?>
							<li>
								<a href="<?php echo esc_url( $privacy_policy_link ); ?>">
									<?php _e( 'Privacy Policy', 'crb' ); ?>
								</a>
							</li>
						<?php endif; ?>
						
						<?php if ( $terms_and_conditions_link = get_field( 'footer_terms_and_conditions_link', 'option' ) ) : ?>
							<li>
								<a href="<?php echo esc_url( $terms_and_conditions_link ); ?>">
									<?php _e( 'Terms & Conditions', 'crb' ); ?>
								</a>
							</li>
						<?php endif; ?>
					</ul><!-- /.list-links -->
				</div><!-- /.footer__bar -->
			</div><!-- /.container -->
		</footer><!-- /.footer -->

	</div><!-- /.wrapper -->
	<?php wp_footer(); ?>
</body>
</html>