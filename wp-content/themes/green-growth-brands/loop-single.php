<?php while ( have_posts() ) : the_post(); ?>  
	<?php
	$post_obj = get_post( get_the_ID() );
	$content = get_extended( $post_obj->post_content );
	?>

	<div class="container">
		<article class="article-large">
			<div class="article__outer">
				<?php if ( is_singular( 'crb_press_release' ) ) : ?>
				<a href="<?php echo get_the_permalink( crb_get_page_ID_by_template( 'templates/press-room.php' ) ); ?>">
					<i class="fas fa-chevron-left"></i>
						<span><?php _e( 'back to press room', 'crb' ); ?></span>
				</a>
				<?php else : ?>
					<a href="<?php echo site_url();?>/blog/">
						<i class="fas fa-chevron-left"></i>					
							<span><?php _e( 'back to news', 'crb' ); ?></span>
					</a>
				<?php endif; ?>
			</div><!-- /.article__outer -->
				
			<div class="article__inner">
				<?php if ( $content['main'] ) : ?>
					<div class="article__head">
						<h4><?php echo $content['main']; ?></h4>
					</div><!-- /.article__head -->
				<?php endif; ?>

				<div class="article__content">
                    <?php echo apply_filters( 'the_content', $content['extended'] ); ?>
				</div><!-- /.article__content -->
			</div><!-- /.article__inner -->
		</article><!-- /.article-large -->
	</div><!-- /.container -->

	<?php
	crb_render_fragment( 'blog/loop-single-related-stories' );

    if(is_singular('crb_press_release') ):

        $title = get_field( 'for_investors_investors_kit_title', 22 );
        $text = get_field( 'for_investors_investors_kit_text', 22 );
        $image = get_field( 'for_investors_investors_kit_image', 22 );
        $show_button = get_field( 'show_request_investor_kit_button', 22 );

    ?>

        <section class="section-investor-kit">
            <?php if ( $title ) : ?>
                <div class="section__head">
                    <h2><?php echo esc_html( $title ); ?></h2>
                </div><!-- /.section__head -->
            <?php endif; ?>

            <div class="section__body">
                <div class="container">
                    <div class="row justify-content-between">
                        <div class="col-md-5">
                            <?php if ( $text || $show_button ) : ?>
                                <div class="section__content">
                                    <?php echo crb_content( $text ); ?>

                                    <?php if ( $show_button ) : ?>
                                        <a href="<?php echo get_the_permalink( crb_get_page_ID_by_template( 'templates/investors-kit.php' ) ); ?>" class="btn btn--primary"><?php _e( 'Request Investor Kit', 'crb' ); ?></a>
                                    <?php endif; ?>
                                </div><!-- /.section__content -->
                            <?php endif; ?>
                        </div><!-- /.col-md-5 -->

                        <div class="col-md-6">
                            <?php if ( $image ) : ?>
                                <div class="section__image">
                                    <?php echo wp_get_attachment_image( $image['id'], 'big' ); ?>
                                </div><!-- /.section__image -->
                            <?php endif; ?>
                        </div><!-- /.col-md-5 -->
                    </div><!-- /.row justify-content-between align-items-center -->
                </div><!-- /.container -->
            </div><!-- /.section__body -->
        </section><!-- /.section-investors-kit -->

	<?php endif;

	crb_render_fragment( 'common/in-the-news-slider' );

	crb_render_fragment( 'common/subscribe' );

	crb_render_fragment( 'common/instafeed' );
	?>
<?php endwhile; ?>
