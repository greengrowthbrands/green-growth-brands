<?php
if( function_exists('acf_add_options_page') ) {	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'crbn-theme-options.php',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Social Settings',
		'menu_title'	=> 'Socials',
		'parent_slug'	=> 'crbn-theme-options.php',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'In the News Slider Settings',
		'menu_title'	=> 'In the News Slider',
		'parent_slug'	=> 'crbn-theme-options.php',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Contacts Section Settings',
		'menu_title'	=> 'Contacts',
		'parent_slug'	=> 'crbn-theme-options.php',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Subscribe Section Settings',
		'menu_title'	=> 'Subscribe',
		'parent_slug'	=> 'crbn-theme-options.php',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Testimonials Slider Settings',
		'menu_title'	=> 'Testimonials Slider',
		'parent_slug'	=> 'crbn-theme-options.php',
	));
}
?>