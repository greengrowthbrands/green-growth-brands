<?php

add_action('wp_ajax_crb_get_news_posts', 'crb_get_news_posts');
add_action('wp_ajax_nopriv_crb_get_news_posts', 'crb_get_news_posts');
function crb_get_news_posts(){
	if ( ! isset( $_POST['offset'] ) || ! isset( $_POST['fragment'] ) ) {
		die;
	}

	if ( $_POST['fragment'] == 'loop-news' ) {
		$offset = $_POST['offset'];
		crb_render_fragment( 'blog/loop-news', compact( 'offset' ) );
		die();
	} else {
		$offset = $_POST['offset'];
		$next_posts = true;
		crb_render_fragment( 'in-the-news/news-archive', array( 'offset' => $offset, 'next_posts' => $next_posts ) );
		die();
	}
}

add_action('wp_ajax_crb_get_posts', 'crb_get_posts');
add_action('wp_ajax_nopriv_crb_get_posts', 'crb_get_posts');
function crb_get_posts() {
	if ( ! isset( $_POST['offset'] ) ) {
		die;
	}

	$offset = $_POST['offset'];
	$next_posts = true;
	crb_render_fragment( 'blog/loop-articles-small', array( 'offset' => $offset, 'next_posts' => $next_posts ) );
	die();
}

add_action('wp_ajax_crb_get_press_releases', 'crb_get_press_releases');
add_action('wp_ajax_nopriv_crb_get_press_releases', 'crb_get_press_releases');
function crb_get_press_releases() {
	if ( ! isset( $_POST['offset'] ) ) {
		die;
	}

	$offset = $_POST['offset'];
	$next_posts = true;
	crb_render_fragment( 'press-room/listing', array( 'offset' => $offset, 'next_posts' => $next_posts ) );
	die();
}
?>