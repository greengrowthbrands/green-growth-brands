<?php
$news_posts = get_field( 'in_the_news_featured_news' );

$news_posts = array_splice( $news_posts, 0, 3, true );

if ( ! $news_posts ) {
	return;
}
?>

<?php foreach ( $news_posts as $news_post ) : ?>
	<section class="section section--news-padding-top section--news">
		<div class="container container--large">
			<article class="article">
				<div class="row align-items-center justify-content-between">
					<div class="col-md-6">
						<?php if ( has_post_thumbnail( $news_post->ID ) ) : ?>
							<div class="article__image">
                                <img src="<?php echo get_the_post_thumbnail_url( $news_post->ID,'big' ) ?>);"/>
                            </div><!-- /.article__image -->
						<?php endif; ?>
					</div><!-- /.col-md-6 -->

					<div class="col-md-5">
						<div class="article__body">
							<div class="article__head">
								<p class="article__meta"><?php echo get_the_date( 'F j, Y', $news_post->ID ); ?></p><!-- /.article__meta -->

								<h4 class="article__title">
									<a href="<?php echo get_the_permalink( $news_post->ID ); ?>" target="_blank"><?php echo get_the_title( $news_post->ID ); ?></a>
								</h4><!-- /.article__title -->
							</div><!-- /.article__head -->

							<div class="article__content">
								<?php echo wpautop( get_field( 'excerpt', $news_post->ID ) ); ?>
							</div><!-- /.article__content -->

							<?php $terms = wp_get_post_terms( $news_post->ID, 'crb_news_category' ); ?>

							<?php if ( $terms ) : $term = array_shift( $terms ); ?>
								<div class="article__actions">
									<a href="<?php echo get_term_link( $term ); ?>" class="btn-tag"><?php echo esc_html( $term->name ); ?></a>
								</div><!-- /.article__actions -->
							<?php endif; ?>
						</div><!-- /.article__body -->
					</div><!-- /.col-md-5 -->
				</div><!-- /.row -->
			</article><!-- /.article -->
		</div><!-- /.container container-/-large -->
	</section><!-- /.section -->
<?php endforeach; ?>