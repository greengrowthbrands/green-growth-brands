<?php
$featured_news = get_field( 'in_the_news_featured_news' );


$offset = isset( $offset ) ? $offset : '0';
$next_posts = isset( $next_posts ) ? $next_posts : false;

if ( ! $next_posts ) {
	$news_posts = get_posts( array(
		'post_type' => 'crb_news',
		'posts_per_page' => 10,
		'orderby' => 'date',
		'order' => 'DESC',
		'offset' => $offset
	) );
} else {
	$news_posts = get_posts( array(
		'post_type' => 'crb_news',
		'posts_per_page' => 5,
		'orderby' => 'date',
		'order' => 'DESC',
		'post__not_in' => $featured_news_ids,
		'offset' => $offset
	) );
}

$next_posts_check = get_posts( array(
	'post_type' => 'crb_news',
	'posts_per_page' => '5',
	'orderby' => 'date',
	'order' => 'DESC',
	'offset' => $offset + 5,
) );
?>


<section class="section-news-investors section-news-investors--no-padding">
	<div class="container container--small">
		<div class="section__head">
			<h2><?php _e( 'News Archive', 'crb' ); ?></h2>
		</div><!-- /.section__head -->

		<div class="section__body">
			<div class="articles articles--big">
				<?php foreach ( $news_posts as $news_post ) : ?>
					<div class="article article--small">
						<?php if ( has_post_thumbnail( $news_post->ID ) ) : ?>
							<div class="article__image">
                                <img src="<?php echo get_the_post_thumbnail_url( $news_post->ID, 'article-list-small-square' ); ?>" />
                            </div><!-- /.article__image -->
						<?php endif; ?>

						<div class="article__head">
							<p class="article__meta"><?php echo get_the_date( 'F j, Y', $news_post->ID ); ?></p><!-- /.article__meta -->

							<h5 class="article__title">
								<a href="<?php echo get_the_permalink( $news_post->ID ); ?>" target="_blank"><?php echo get_the_title( $news_post->ID ); ?></a>
							</h5><!-- /.article__title -->
						</div><!-- /.article__head -->

						<?php $terms = wp_get_post_terms( $news_post->ID, 'crb_news_category' ); ?>

						<?php if ( $terms ) : $term = array_shift( $terms ); ?>
							<div class="article__actions">
								<a href="<?php echo get_term_link( $term ); ?>" class="btn-tag"><?php echo esc_html( $term->name ); ?></a>
							</div><!-- /.article__actions -->
						<?php endif; ?>
					</div><!-- /.article -->
				<?php endforeach; ?>
			</div><!-- /.articles -->
		</div><!-- /.section__content -->

		<div class="section__actions">
			<?php if ( $next_posts_check ) : ?>
				<a class="btn btn--primary btn--trasparent btn--large news-load" data-fragment="news-archive" data-offset="10" href="#"><?php _e( 'load more', 'crb' ); ?></a>
			<?php endif; ?>
		</div><!-- /.section__actions -->

		<div class="section__foot section__foot--icon">
			<span></span>

			<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/ico-circle-leaf.svg" alt="">

			<span></span>
		</div><!-- /.section__foot -->
	</div><!-- /.container container-/-small -->
</section><!-- /.section-news -->