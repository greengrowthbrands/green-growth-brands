<?php
$news_posts = get_field( 'in_the_news_featured_news' );

$news_posts = array_splice( $news_posts, 3, 8, true );

if ( ! $news_posts ) {
	return;
}
?>

<section class="section-news-investors section-news-investors--no-padding">
	<div class="container container--small">
		<div class="section__body">
			<div class="articles articles--big">
				<?php foreach ( $news_posts as $news_post ) : ?>
					<div class="article article--small">
						<div class="article__image">
                            <img src="<?php echo get_the_post_thumbnail_url( $news_post->ID, 'article-list-small-square' ); ?>" />
                        </div><!-- /.article__image -->

						<div class="article__head">
							<p class="article__meta"><?php echo get_the_date( 'F j, Y', $news_post->ID ); ?></p><!-- /.article__meta -->

							<h5 class="article__title">
								<a href="<?php echo get_the_permalink( $news_post->ID ); ?>" target="_blank"><?php echo get_the_title( $news_post->ID ); ?></a>
							</h5><!-- /.article__title -->
						</div><!-- /.article__head -->

						<?php $terms = wp_get_post_terms( $news_post->ID, 'crb_news_category' ); ?>

						<?php if ( $terms ) : $term = array_shift( $terms ); ?>
							<div class="article__actions">
								<a href="<?php echo get_term_link( $term ); ?>" class="btn-tag"><?php echo esc_html( $term->name ); ?></a>
							</div><!-- /.article__actions -->
						<?php endif; ?>
					</div><!-- /.article -->
				<?php endforeach; ?>
			</div><!-- /.articles -->
		</div><!-- /.section__content -->
	</div><!-- /.container container-/-small -->
</section><!-- /.section-news -->