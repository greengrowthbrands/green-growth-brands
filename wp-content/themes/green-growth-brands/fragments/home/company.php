<?php
$content = get_field( 'homepage_company_content' );
$images = get_field( 'homepage_company_images' );

if ( ! $content && ! $images ) {
	return;
}
?>

<section class="section-company" id="section-company" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/svg/single-leaf.svg);">
	<div class="container">
		<div class="section__body">
			<div class="row">
				<div class="col-md-6">
					<?php if ( $content ) : ?>
						<div class="section__content">
							<?php if ( $content['content'] ) : ?>
								<div class="section__entry">
									<?php echo crb_content( $content['content'] ); ?>
								</div><!-- /.section__entry -->
							<?php endif; ?>
							
							<?php if ( $content['button_label'] && $content['button_link'] ) : ?>
								<div class="section__actions">
									<a href="<?php echo esc_url( $content['button_link'] ); ?>" class="btn btn--primary"><?php echo esc_html( $content['button_label'] ); ?></a>
								</div><!-- /.section__actions -->
							<?php endif; ?>
						</div><!-- /.section__content -->
					<?php endif; ?>
				</div><!-- /.col-md-6 -->

				<div class="col-md-6">
					<?php if ( $images ) : ?>
						<aside class="section__aside">
							<div class="section__image-grid" style="background-image: url(<?php echo wp_get_attachment_url( $images['main_image']['id'] ) ?>);" data-aos="fade-left">

								<?php if ( $images['secondary_image'] ) : ?>
									<div class="section__image" data-aos="fade-left">
										<?php echo wp_get_attachment_image( $images['secondary_image']['id'], 'big' ); ?>
									</div><!-- /.section__image -->
								<?php endif; ?>
							</div><!-- /.section__image -->
						</aside><!-- /.section__aside -->
					<?php endif; ?>
				</div><!-- /.col-md-6 -->
			</div><!-- /.row -->
		</div><!-- /.section__body -->
	</div><!-- /.container -->
</section><!-- /.section-company -->