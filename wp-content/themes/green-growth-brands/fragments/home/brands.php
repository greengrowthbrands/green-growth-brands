<?php
$title = get_field( 'homepage_brands_title' );
$sub_title = get_field( 'homepage_brands_sub_title' );
$brands = get_field( 'homepage_brands_slides' );

if ( ! $title && ! $sub_title && ! $brands ) {
	return;
}
?>

<section class="section-brands" style="">
	<?php if ( $title || $sub_title ) : ?>
		<header class="section__head">
			<?php if ( $title ) : ?>
				<h2 class="section__title"><?php echo esc_html( $title ); ?></h2><!-- /.section__title -->
				
				<?php echo wpautop( $sub_title ); ?>
		<?php endif; ?>
	</header><!-- /.section__head -->
	<?php endif; ?>
	
	<?php if ( $brands ) : ?>
		<div class="section__body">
			<div class="tabs-brands">
				<div class="tabs__head">
					<nav class="tabs__nav">
						<ul>
							<?php foreach ( $brands as $i => $brand ) : ?>
								<li class="<?php echo $i == 0 ? 'current' : '' ?>">
									<a id="<?php echo $i; ?>">
										<?php echo esc_html( $brand->post_title ); ?>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
					</nav><!-- /.tabs__nav -->

					<span class="target"></span>
				</div><!-- /.tabs__head -->
				
				<div class="tabs__body">
					<?php foreach ( $brands as $brand ) : ?>
						<div class="brand">
							<div class="row">
								<div class="col-md-6">
									<?php if ( $images = get_field( 'brand_images', $brand->ID ) ) : ?>
										<div class="brand__aside">
											<?php if ( $images['main_image'] ) : ?>
												<div class="brand__image-grid" style="background-image: url(<?php echo wp_get_attachment_url( $images['main_image']['id'] ); ?>);" data-aos="fade-right">
													
													<?php if ( $images['left_image'] ) : ?>
														<div class="brand__image" style="background-image: url(<?php echo wp_get_attachment_url( $images['left_image']['id'] ) ?>);" data-aos="fade-right"></div><!-- 
														/.brand__image -->
													<?php endif; ?>

													<?php if ( $images['right_image'] ) : ?>
														<div class="brand__image" style="background-image: url(<?php echo wp_get_attachment_url( $images['right_image']['id'] ); ?>);" data-aos="fade-right"></div><!-- /.brand__image -->
													<?php endif; ?>
												</div><!-- /.brand__image-grid -->
											<?php endif; ?>
										</div><!-- /.brand__aside -->
									<?php endif; ?>
								</div><!-- /.col-md-6 -->
								
								<div class="col-md-6">
									<div class="brand__content" data-aos="fade-in">
										<?php if ( has_post_thumbnail( $brand->ID ) ) : ?>
											<div class="brand__icon">
												<img src="<?php echo get_the_post_thumbnail_url( $brand->ID ); ?>" alt="" class="logo-camp">
											</div><!-- /.brand__icon -->
										<?php endif; ?>
										
										<?php if ( $brand->post_content ) : ?>
											<div class="brand__entry">
												<?php echo wpautop( get_the_excerpt( $brand->ID ) ); ?>
											</div><!-- /.brand__entry -->
										<?php endif; ?>
										
										<?php
										$button_label = get_field( 'button_label', $brand->ID );
										$button_url = get_field( 'button_url', $brand->ID )
										?>
										<?php if ( $button_label && $button_url ) : ?>
											<div class="brand__actions hidden-xs">
												<a href="<?php echo esc_url( $button_url ); ?>" class="btn btn--primary">
													<?php echo esc_html( $button_label ); ?>
												</a>
											</div><!-- /.brand__actions -->	
										<?php endif; ?>
									</div><!-- /.brand__content -->
								</div><!-- /.col-md-6 -->
							</div><!-- /.row -->
						</div><!-- /.brand -->	
					<?php endforeach; ?>
				</div><!-- /.tabs__body -->
			</div><!-- /.tabs -->
		</div><!-- /.section__body -->
	<?php endif; ?>
</section><!-- /.section-brands -->