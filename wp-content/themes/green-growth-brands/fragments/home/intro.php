<?php
$title = get_field( 'homepage_intro_title' );
$sub_title = get_field( 'homepage_intro_sub_title' );

if ( $title && ! $sub_title ) {
	return;
}
?>

<div class="intro intro-no-backgound" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/svg/ico-leaf-large.svg);">
	<div class="container">
		<div class="intro__content">
			<?php if ( $title ) : ?>
				<h1><?php echo esc_html( $title ); ?></h1>
			<?php endif; ?>

			<p><?php echo $sub_title; ?></p>
		</div><!-- /.intro__content -->
		
		<div class="intro__actions">
			<a href="#section-company" class="link link-scroll">
				<span class="ico-arrow-down">
					
				</span>
			</a>
		</div><!-- /.intro__actions -->
	</div><!-- /.container -->
</div><!-- /.intro -->