<?php 
$title = get_field( 'homepage_team_members_slider_title' );
$members = get_field( 'homepage_slider_team_members' );

if ( ! $title && ! $members ) {
	return;
}
?>

<section class="section-team">
	<?php if ( $title ) : ?>
		<header class="section__head">
			<h2 class="section__title"><?php echo esc_html( $title ); ?></h2><!-- /.section__title -->	
		</header><!-- /.section__head -->
	<?php endif; ?>
	
	<?php if ( $members ) : ?>
		<div class="section__body">
			<div class="tabs-team tabs-team--home">
				<div class="tabs__head">
					<nav class="tabs__nav">
						<ul>
							<?php foreach ( $members as $i => $member ) : ?>
								<li class="<?php echo $i == 0 ? 'current' : '' ?>">
									<a id="<?php echo $i ?>">
										<?php echo esc_html( $member->post_title ); ?>
									</a>
								</li>
							<?php endforeach; ?>
						</ul>
					</nav><!-- /.tabs__nav -->

					<span class="target"></span>
				</div><!-- /.tabs__head -->
				
				<div class="tabs__body">
					<?php foreach ( $members as $member ) : ?>
						<div class="member-entry">
							<div class="member">
								<div class="member__inner">
									<?php if ( has_post_thumbnail( $member->ID ) ) : ?>
										<div class="member__aside" data-aos="fade-in">
											<div class="member__image" style="background-image: url(<?php echo get_the_post_thumbnail_url( $member->ID ); ?>);"></div><!-- /.member__image -->
										</div><!-- /.member__aside -->
									<?php endif; ?>
									
									<div class="member__content" data-aos="fade-in">
										<div class="member__entry">
											<h4>
												<?php echo esc_html( $member->post_title ); ?>
											</h4>
											
											<?php if ( $position = get_field( 'team_member_position', $member ) ) : ?>
												<h6><?php echo esc_html( $position ); ?></h6>
											<?php endif; ?>
											
											<?php echo wpautop( $member->post_content ); ?>
										</div><!-- /.member__entry -->
										
										<div class="member__actions">
											<a href="<?php echo get_the_permalink( crb_get_page_ID_by_template( 'templates/our-team.php' ) ); ?>" class="btn btn--primary">
												<?php _e( 'Meet the team', 'crb' ); ?>
											</a>
										</div><!-- /.member__actions -->
									</div><!-- /.member__content -->
								</div><!-- /.memeber__inner -->
							</div><!-- /.member -->	
						</div><!-- /.member-entry -->
					<?php endforeach; ?>
				</div><!-- /.tabs__body -->
			</div><!-- /.tabs -->
		</div><!-- /.section__body -->
	<?php endif; ?>
</section><!-- /.section-brands -->