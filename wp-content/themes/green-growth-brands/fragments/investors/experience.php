<?php
$title = get_field( 'for_investors_experience_title' );
$text = get_field( 'for_investors_experience_text' );
$logos = get_field( 'for_investors_experience_logos' );

if ( ! $title && ! $text && ! $logos ) {
	return;
}
?>

<section class="section-experience">
	<div class="container">
		<div class="row justify-content-between align-items-center">
			<div class="col-md-5">
				<?php if ( $title ) : ?>
					<div class="section__head">
						<h3><?php echo esc_html( $title ); ?></h3>
					</div><!-- /.section__head -->
				<?php endif; ?>
			</div><!-- /.col-md-5 -->

			<div class="col-md-5">
				<?php if ( $text ) : ?>
					<div class="section__body">
						<?php echo wpautop( $text ); ?>
					</div><!-- /.section__body -->
				<?php endif; ?>
			</div><!-- /.col-md-5 -->
		</div><!-- /.row justify-content-between align-items-center -->

		<?php if ( $logos ) : ?>
			<div class="section__foot">
				<ul>
					<?php foreach ( $logos as $logo ) : ?>
						<li>
							<a href="<?php echo esc_url( $logo['link'] ) ?>" target="_blank">
								<img src="<?php echo wp_get_attachment_url( $logo['logo']['id'] ); ?>" alt="">
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div><!-- /.section__foot -->
		<?php endif; ?>

		<div class="section__outer">
			<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/ico-leaf-large.svg" alt="" class="section__outer--image section__outer--image-1">
		</div><!-- /.section__outer -->
	</div><!-- /.container -->
</section><!-- /.section-expirience -->