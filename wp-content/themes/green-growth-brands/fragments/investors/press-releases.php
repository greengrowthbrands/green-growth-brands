<?php
$title = get_field( 'for_investors_press_releases_title' );
$order = get_field( 'for_investors_press_releases_order' );

if ( $order == 'date' ) {
	$press_releases = get_posts( array(
		'post_type' => 'crb_press_release',
		'posts_per_page' => 2,
		'orderby' => 'date',
		'order' => 'DESC'
	) );
} else {
	$press_releases = get_field( 'for_investors_press_releases' );
}

if ( ! $press_releases ) {
	return;
}
?>

<section class="section-press-releases">
	<div class="container container--small">
		<?php if ( $title ) : ?>
			<div class="section__head">
				<h2><?php echo esc_html( $title ); ?></h2>
			</div><!-- /.section__head -->
		<?php endif; ?>
		
		<div class="section__body">
			<div class="releases">
				<?php foreach ( $press_releases as $release ) : ?>
					<div class="release">
						<div class="release__head">
							<p class="release__meta"><?php echo get_the_date( 'F j, Y', $release->ID ); ?></p><!-- /.article__meta -->

							<h4 class="release__title">
								<a href="<?php echo get_the_permalink( $release->ID ); ?>"><?php echo get_the_title( $release->ID ); ?></a>
							</h4><!-- /.release__title -->
						</div><!-- /.release__head -->

						<div class="release__body">
							<?php $content = get_extended( $release->post_content ); echo wpautop( $content['main'] ); ?>
						</div><!-- /.release__body -->
					</div><!-- /.release -->
				<?php endforeach; ?>
			</div><!-- /.releases -->
		</div><!-- /.section__body -->

		<div class="section__actions">
			<a href="<?php echo get_the_permalink( crb_get_page_ID_by_template( 'templates/press-room.php' ) ); ?>" class="btn btn--primary"><?php _e( 'Press Room', 'crb' ); ?></a>
		</div><!-- /.section__actions -->

		<div class="section__foot section__foot--icon">
			<span></span>

			<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/ico-circle-leaf.svg" alt="">

			<span></span>
		</div><!-- /.section__foot -->
	</div><!-- /.container container-/-small -->
</section><!-- /.section-press-releases -->