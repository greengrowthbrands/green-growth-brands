<?php
$title = get_field( 'for_investors_news_title' );
$order = get_field( 'for_investors_news_order' );

if ( $order == 'date' ) {
	$news = get_posts( array(
		'post_type' => 'crb_news',
		'posts_per_page' => 5,
		'orderby' => 'date',
		'order' => 'DESC'
	) );
} else {
	$news = get_field( 'for_investors_news' );
}

if ( ! $news ) {
	return;
}
?>

<section class="section-news-investors">
	<div class="container container--small">
		<?php if ( $title ) : ?>
			<div class="section__head">
				<h2><?php echo esc_html( $title ); ?></h2>
			</div><!-- /.section__head -->
		<?php endif; ?>

		<div class="section__body">
			<div class="articles articles--big">
				<?php foreach ( $news as $news_post ) : ?>
					<div class="article article--small">
						<div class="article__image">
                             <img src="<?php echo get_the_post_thumbnail_url( $news_post->ID, 'large' ) ?>" />
                        </div><!-- /.article__image -->

						<div class="article__head">
							<p class="article__meta"><?php echo get_the_date( 'F j, Y', $news_post->ID ); ?></p><!-- /.article__meta -->

							<h5 class="article__title">
								<a href="<?php echo get_the_permalink( $news_post->ID ); ?>" target="_blank"><?php echo get_the_title( $news_post->ID ); ?></a>
							</h5><!-- /.article__title -->
						</div><!-- /.article__head -->

						<?php $terms = wp_get_post_terms( $news_post->ID, 'crb_news_category' ); ?>

						<?php if ( $terms ) : $term = array_shift( $terms ); ?>
							<div class="article__actions">
								<a href="<?php echo get_term_link( $term ); ?>" class="btn-tag"><?php echo esc_html( $term->name ); ?></a>
							</div><!-- /.article__actions -->
						<?php endif; ?>
					</div><!-- /.article -->
				<?php endforeach; ?>
			</div><!-- /.articles -->
		</div><!-- /.section__content -->

		<div class="section__actions">
			<a href="<?php echo get_the_permalink( crb_get_page_ID_by_template( 'templates/in-the-news.php' ) ); ?>" class="btn btn--primary btn--trasparent btn--large"><?php _e( 'more from the newsroom', 'crb' ) ?></a>
		</div><!-- /.section__actions -->
	</div><!-- /.container container-/-small -->
</section><!-- /.section-news -->