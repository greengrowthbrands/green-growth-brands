<?php
    $disable_investor_kit_download_row = get_field( 'for_investors_download_kit_disable' );
?>

<?php if ($disable_investor_kit_download_row == 0):?>
<section class="section-download white-bg">
    <div class="container container--small">
        <div class="row">
            <div class="col-md-6">
                <?php if ( $content = get_field( 'for_investors_download_kit' ) ) : ?>
                    <div class="section__head">
                        <?php echo crb_content( $content ); ?>
                    </div><!-- /.section__head -->
                <?php endif; ?>

                <?php if ( $image = get_field( 'for_investors_download_kit_image' ) ) : ?>
                    <div class="section__image">
                        <?php echo wp_get_attachment_image( $image['id'], 'big' ); ?>
                    </div><!-- /.section__image -->
                <?php endif; ?>
            </div><!-- /.col-md-6 -->

            <div class="col-md-5">
                <?php if ( $form = get_field( 'for_investors_download_kit_form' ) ) : ?>
                    <div class="form">
                        <div class="form__head">
                            <h5><?php _e( 'Please complete the form below.', 'crb' ); ?></h5>
                        </div><!-- /.form__head -->

                        <div class="form__content">
                            <?php echo do_shortcode( $form ); ?>
                        </div><!-- /.form__content -->
                    </div><!-- /.form -->
                <?php endif; ?>
            </div><!-- /.col-md-5 -->
        </div><!-- /.justify-content-center align-items-center -->
    </div><!-- /.container-/-small -->
</section><!-- /.section-download -->
<?php endif;?>