<?php
$title = get_field( 'for_investors_investors_kit_title' );
$text = get_field( 'for_investors_investors_kit_text' );
$image = get_field( 'for_investors_investors_kit_image' );
$show_button = get_field( 'show_request_investor_kit_button' );
$disable_investor_kit_row = get_field( 'investors_kit_disable_row' );

if ( ! $title && ! $text && ! $image ) {
	return;
}
?>

<?php if ( $disable_investor_kit_row == 0 ):?>
<section class="section-investor-kit">
	<?php if ( $title ) : ?>
		<div class="section__head">
			<h2><?php echo esc_html( $title ); ?></h2>
		</div><!-- /.section__head -->
	<?php endif; ?>

	<div class="section__body">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-md-5">
					<?php if ( $text || $show_button ) : ?>
						<div class="section__content">
							<?php echo crb_content( $text ); ?>

							<?php if ( $show_button ) : ?>
								<a href="<?php echo get_the_permalink( crb_get_page_ID_by_template( 'templates/investors-kit.php' ) ); ?>" class="btn btn--primary"><?php _e( 'Request Investor Kit', 'crb' ); ?></a>
							<?php endif; ?>
						</div><!-- /.section__content -->
					<?php endif; ?>
				</div><!-- /.col-md-5 -->

				<div class="col-md-6">
					<?php if ( $image ) : ?>
						<div class="section__image">
							<?php echo wp_get_attachment_image( $image['id'], 'big' ); ?>
						</div><!-- /.section__image -->		
					<?php endif; ?>
				</div><!-- /.col-md-5 -->
			</div><!-- /.row justify-content-between align-items-center -->
		</div><!-- /.container -->
	</div><!-- /.section__body -->
</section><!-- /.section-investors-kit -->
<?php endif;?>
