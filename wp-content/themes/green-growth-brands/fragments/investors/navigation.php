<?php
$main_page_id = crb_get_page_ID_by_template( 'templates/for-investors.php' );
$child_pages = get_posts( array(
	'post_type' => 'page',
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'post_parent' => $main_page_id
) );

if ( ! $child_pages ) {
	return;
}
?>

<div class="tabs-investors">
	<div class="shell">
		<ul>
			<li class="<?php echo get_the_ID() == $main_page_id ? 'current' : '' ?>">
				<a href="<?php echo get_the_permalink( $main_page_id ) ?>"><?php _e( 'Overview', 'crb' ) ?></a>
			</li>

			<?php foreach ( $child_pages as $page ) : ?>
				<li class="<?php echo get_the_ID() == $page->ID ? 'current' : '' ?>">
					<a href="<?php echo get_the_permalink( $page->ID ); ?>"><?php echo esc_html( $page->post_title ); ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div><!-- /.shell -->
</div><!-- /.tabs-investors -->