<?php
$text = get_field( 'contacts_text', 'option' );
$general_contacts = get_field( 'general_contacts', 'option' );
$contacts = get_field( 'contacts', 'option' );
$form = get_field( 'contacts_form', 'option' );

if ( ! $text && ! $general_contacts && ! $contacts && ! $form ) {
	return;
}
?>

<section class="section section--contact">
	<div class="container container--large">
		<div class="row justify-content-between">
			<div class="col-md-6">
				<?php if ( $text ) : ?>
					<div class="section__head text-left">
						<h3><?php echo $text; ?></h3>
					</div><!-- /.section__head -->
				<?php endif; ?>

				<div class="section__content">
					<div class="row">
						<?php if ( $general_contacts ) : ?>
							<div class="col-md-6">
								<div class="contact">
									<div class="contact__head">
										<h4><?php _e( 'General', 'crb' ); ?></h4>
									</div><!-- /.contact__head -->

									<div class="contact__content">
										<ul>
											<?php if ( $general_contacts['email'] ) : ?>
												<li>
													<a href="mailto:<?php echo esc_html( antispambot( $general_contacts['email'] ) ); ?>"><?php echo esc_html( antispambot( $general_contacts['email'] ) ); ?></a>
												</li>
											<?php endif; ?>
											
											<li>
												<?php if ( $general_contacts['phone'] ) : ?>
													<a href="tel:<?php echo $general_contacts['phone']; ?>">
														<strong>p /</strong>

														<span><?php echo esc_html( $general_contacts['phone'] ) ?></span>
													</a>
												<?php endif; ?>

												<?php if ( $general_contacts['fax'] ) : ?>
													<a href="tel:<?php echo $general_contacts['fax']; ?>">
														<strong>f /</strong>

														<span><?php echo esc_html( $general_contacts['fax'] ); ?></span>
													</a>
												<?php endif; ?>
											</li>
											
											<?php if ( $general_contacts['address'] ) : ?>
												<li>
													<address>
														<?php echo $general_contacts['address']; ?>
													</address>
												</li>
											<?php endif; ?>
										</ul>
									</div><!-- /.contact__content -->
								</div><!-- /.contacts -->
							</div><!-- /.col-md-6 -->
						<?php endif; ?>

						<?php if ( $contacts['emails'] ) : ?>
							<div class="col-md-6">
								<?php foreach ( $contacts['emails'] as $contact ) : ?>
									<div class="contact">
										<?php if ( $contact['title'] ) : ?>
											<div class="contact__head">
												<h4><?php echo esc_html( $contact['title'] ); ?></h4>
											</div><!-- /.contact__head -->
										<?php endif; ?>

										<?php if ( $contact['email'] ) : ?>
											<div class="contact__content">
												<a href="mailto:<?php echo esc_html( antispambot( $contact['email'] ) ); ?>"><?php echo esc_html( antispambot( $contact['email'] ) ); ?></a>
											</div><!-- /.contact__content -->
										<?php endif; ?>
									</div><!-- /.contacts -->
								<?php endforeach; ?>
							</div><!-- /.col-md-6 -->
						<?php endif; ?>
					</div><!-- /.row -->
				</div><!-- /.section__content -->
			</div><!-- /.col-md-6 -->

			<div class="col-md-5">
				<div class="form">
					<div class="form__head">
						<h3><?php _e( 'How can we help?', 'crb' ); ?></h3>
					</div><!-- /.form__head -->

					<div class="form__content">
						<?php echo do_shortcode( $form ); ?>
					</div><!-- /.form__content -->
				</div><!-- /.form -->
			</div><!-- /.col-md-5 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.section -->