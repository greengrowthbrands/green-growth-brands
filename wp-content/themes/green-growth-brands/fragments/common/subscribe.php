<?php
$title = get_field( 'subscribe_title', 'option' );
$sub_title = get_field( 'subscribe_sub_title', 'option' );
$form = get_field( 'subscribe_form', 'option' );
$text = get_field( 'subscribe_privacy_text', 'option' );
?>

<section class="section-subscribe" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/svg/ico-leaf-large-2.svg);">
	<div class="container">
		<header class="section__head">
			<?php if ( $title ) : ?>
				<h3 class="section__title"><?php echo esc_html( $title ); ?></h3><!-- /.section__title -->
			<?php endif; ?>

			<?php echo wpautop( $sub_title ); ?>
		</header><!-- /.section__head -->
		
		<div class="section__body">
			<div class="subscribe">
				<?php echo do_shortcode( $form ); ?>
			</div><!-- /.subscribe -->

			<?php if ( $text ) : ?>
				<div class="section__entry">
					<?php echo wpautop( $text ); ?>
				</div><!-- /.section__entry -->
			<?php endif; ?>
		</div><!-- /.section__body -->
	</div><!-- /.container -->
</section><!-- /.section-subscribe -->