<section class="section-slider">
	<div class="container">
		<div class="section__body">
			<div class="slider-ethos">
				<div class="slider__clip">
					<div class="slider__slides">
						<div class="slider__slide">
							<div class="slider__slide-content">
								<h5>
									Customers find that we’re just what they want.
								</h5>

								<h4>
									A deep understanding of our customers drives everything we do. Time, place, product, message, interaction—it’s all intentionally designed to meet customer needs and exceed customer expectations.
								</h4>
							</div><!-- /.slider__slide-content -->	
						</div><!-- /.slider__slide -->


						<div class="slider__slide">
							<div class="slider__slide-content">
								<h5>CUSTOMERS FEEL AT HOME WITH US.</h5>
								
								<h4>We are more than memorable to customers, we are familiar to them. Our brands are trusted, beloved and inspire advocacy.</h4>
							</div><!-- /.slider__slide-content -->	
						</div><!-- /.slider__slide -->


						<div class="slider__slide">
							<div class="slider__slide-content">
								<h5>WE HAVE DEEP EXPERIENCE—AND PROVEN RESULTS—WINNING CUSTOMERS. </h5>
								
								<h4>As specialty retail veterans, we’ve created some of the world’s most popular retail brands. We know what works, and how to scale it quickly.</h4>
							</div><!-- /.slider__slide-content -->	
						</div><!-- /.slider__slide -->
					</div><!-- /.slider__slides -->
				</div><!-- /.slider__clip -->
			</div><!-- /.slider -->
		</div><!-- /.section__body -->
	</div><!-- /.container -->
</section><!-- /.section-slider -->