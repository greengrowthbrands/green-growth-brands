<?php
$slides = get_field( 'news_slides', 'option' );

if ( ! $slides ) {
	return;
}
?>

<section class="section-news">
	<div class="container">
		<div class="section__body">
			<div class="slider-news">
				<div class="slider__clip">
					<div class="slider__slides">
						<?php foreach ( $slides as $slide ) : ?>
							<div class="slider__slide">
								<div class="slider__slide-content">
									<h5>
										<?php echo esc_html( $slide->post_title ); ?>
									</h5>

									<?php echo wpautop( $slide->post_content ); ?>
									
									<?php if ( $link = get_field( 'link', $slide->ID ) ) : ?>
										<a href="<?php echo esc_url( $link ); ?>" class="btn btn--primary">
											<?php _e( 'Read more', 'crb' ); ?>
										</a>
									<?php endif; ?>
								</div><!-- /.slider__slide-content -->
							</div><!-- /.slider__slide -->
						<?php endforeach; ?>
					</div><!-- /.slider__slides -->
				</div><!-- /.slider__clip -->

				<div class="slider__paging">
					<ul>
						<?php foreach ( $slides as $i => $slide ) : ?>
							<li class="<?php echo $i == 0 ? 'current' : '' ?>">
								<a href="#">
									<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $slide->ID ) ); ?>" alt="">
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div><!-- /.slider__paging -->
			</div><!-- /.slider -->
		</div><!-- /.section__body -->
	</div><!-- /.container -->
</section><!-- /.section-news -->