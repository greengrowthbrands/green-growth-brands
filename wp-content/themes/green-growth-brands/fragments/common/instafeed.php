<?php
$insta_posts_query = get_posts( [
	'post_type'      => 'crb_fa_entry', // or use constant CRB_FA_ENTRY_POST_TYPE
	'posts_per_page' => 8,
	'meta_query' => array(
		array(
			'key' => '_crb_fa_entry_feed_source',
			'value' => 'instagram'
		)
	)
] );

if ( ! $insta_posts_query ) {
	return;
}
?>

<section class="section-gallery">
	<div class="section__body">
		<ul class="list-gallery" id="instafeed">
			<?php foreach ( $insta_posts_query as $post ) : ?>
				<li>
					<a href="<?php echo get_post_meta( $post->ID, '_crb_fa_entry_link', true ); ?>">
						<?php $image = get_post_meta( $post->ID , '_crb_fa_custom_images', true); ?>

						<img src="<?php echo $image->low_resolution->url ?>" alt="">
					</a>
				</li>
			<?php endforeach; ?>
		</ul><!-- /.list-gallery -->
	</div><!-- /.section__body -->
</section><!-- /.section-gallery -->