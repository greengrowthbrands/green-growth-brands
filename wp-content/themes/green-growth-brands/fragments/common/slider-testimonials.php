<?php
$testimonials = get_field( 'testimonials_slides', 'option' );

if ( ! $testimonials ) {
	return;
}
?>

<section class="section-slider">
	<div class="container">
		<div class="section__body">
			<div class="slider-ethos">
				<div class="slider__clip">
					<div class="slider__slides">
						<?php foreach ( $testimonials as $testimonial ) : ?>
							<div class="slider__slide">
								<div class="slider__slide-content">
									<?php if ( $testimonial->post_title ) : ?>
										<h5><?php echo esc_html( $testimonial->post_title ); ?></h5>
									<?php endif; ?>
								
									<?php if ( $testimonial->post_content ) : ?>
										<h4><?php echo esc_html( $testimonial->post_content ); ?></h4>
									<?php endif; ?>
								</div><!-- /.slider__slide-content -->	
							</div><!-- /.slider__slide -->
						<?php endforeach; ?>
					</div><!-- /.slider__slides -->
				</div><!-- /.slider__clip -->
			</div><!-- /.slider -->
		</div><!-- /.section__body -->
	</div><!-- /.container -->
</section><!-- /.section-slider -->