<?php
global $template;
$template_name = basename( $template );
$title = '';
$sub_title = '';
$classes = [ 'intro' ];

switch ( $template_name ) {
	case 'for-investors.php':
		$title = get_field( 'for_investors_intro_title' );
		$sub_title = get_field( 'for_investors_intro_sub_title' );
		$image = get_the_post_thumbnail_url();
		break;

	case 'investors-kit.php':
		$title = get_field( 'investors_kit_intro_title' );
		$sub_title = get_field( 'investors_kit_intro_sub_title' );
		$image = get_the_post_thumbnail_url();
		break;

	case 'press-room.php':
		$title = get_field( 'press_room_intro_title' );
		$sub_title = get_field( 'press_room_intro_sub_title' );
		$image = get_the_post_thumbnail_url();
		break;

	case 'in-the-news.php':
		$title = get_field( 'in_the_news_intro_title' );
		$sub_title = get_field( 'in_the_news_intro_sub_title' );
		$image = get_the_post_thumbnail_url();
		break;

	case 'contact.php':
		$title = get_field( 'contact_page_intro_title' );
		$sub_title = get_field( 'contact_page_intro_sub_title' );
		$image = get_the_post_thumbnail_url();
		$classes[] = 'intro--blog';
		break;

	case 'our-story.php':
		$title = get_field( 'our_story_intro_title' );
		$sub_title = get_field( 'our_story_intro_sub_title' );
		$image = get_the_post_thumbnail_url();
		break;

	case 'page.php':
		$title = crb_get_title();
		$image = get_the_post_thumbnail_url();
		break;
	
	default:
		# code...
		break;
}

if(is_404()):
    $title = 'Not Found';
    $image = get_the_post_thumbnail_url();
endif;
?>

<div class="<?php echo implode( ' ', $classes ); ?>">
	<div class="container">
		<div class="intro__image" style="background-image: url(<?php echo $image; ?>);"></div><!-- /.intro__image -->
		
		<div class="intro__content">
			<?php if ( $title ) : ?>
				<h1><?php echo esc_html( $title ); ?></h1>
			<?php endif; ?>
				
			<?php echo wpautop( $sub_title ); ?>
		</div><!-- /.intro__content -->
	</div><!-- /.container -->
</div><!-- /.intro -->