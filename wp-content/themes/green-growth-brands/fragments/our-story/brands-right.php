<?php
if ( ! $section ) {
	return;
}
?>

<section class="section-brands-right pb-5 section-brands-right--story">
	<div class="container container--small">
		<div class="row align-items-center justify-content-start">
			<div class="col-md-6">
				<?php if ( $section['content'] ) : ?>
					<div class="section__content">
						<div class="section__entry">
							<?php if ( $section['content']['title'] ) : ?>
								<h3><?php echo esc_html( $section['content']['title'] ) ?></h3>
							<?php endif; ?>
							
							<?php if ( $section['content']['sub_title'] ) : ?>
								<h4><?php echo esc_html( $section['content']['sub_title'] ); ?></h4>
							<?php endif; ?>

							<?php echo wpautop( $section['content']['text'] ); ?>
						</div><!-- /.section__entry -->
						
						<?php if ( $section['content']['button_label']  && $section['content']['button_url']) : ?>
							<div class="section__actions">
								<a href="<?php echo esc_url( $section['content']['button_url'] ) ?>" class="btn btn--primary"><?php echo esc_html( $section['content']['button_label'] ); ?></a>
							</div><!-- /.section__actions -->
						<?php endif; ?>
					</div><!-- /.section__content -->
				<?php endif; ?>
			</div><!-- /.col-md-6 -->

			<div class="col-md-6">
				<?php if ( $section['images'] ) : ?>
					<aside class="section__aside">
						<?php if ( $section['images']['main_image'] ) : ?>
							<div class="section__image-grid" style="background-image: url(<?php echo wp_get_attachment_url( $section['images']['main_image']['id'] ); ?>);" data-aos="fade-left">

								<?php if ( $section['images']['secondary_image'] ) : ?>
									<div class="section__image section__image--top" style="background-image: url(<?php echo wp_get_attachment_url( $section['images']['secondary_image']['id'] ) ?>);" data-aos="fade-left"></div><!-- /.section__image -->
								<?php endif; ?>
							</div><!-- /.section__image -->
						<?php endif; ?>
					</aside><!-- /.section__aside -->
				<?php endif; ?>
			</div><!-- /.col-md-6 -->
		</div><!-- /.row -->
	</div><!-- /.conrainer -->
</section><!-- /.section-brands-right -->