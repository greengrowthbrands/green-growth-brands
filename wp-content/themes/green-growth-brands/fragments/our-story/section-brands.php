<?php
$sections = get_field( 'text_and_images' );

foreach ( $sections as $i => $section ) {
	$i % 2 == 0 ? crb_render_fragment( 'our-story/brands-right', compact( 'section' ) ) : crb_render_fragment( 'our-story/brands-left', compact( 'section' ) );
}
?>