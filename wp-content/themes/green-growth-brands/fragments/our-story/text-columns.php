<?php
$title = get_field( 'our_story_columns_title' );
$text = get_field( 'our_story_columns_text' );

if ( ! $title && ! $text ) {
	return;
}
?>

<section class="section-brands">
	<div class="container container--large">
		<div class="section__head section__head--large">
			<div class="row justify-content-center">
				<div class="col-md-5 text-left"> 
					<?php if ( $title ) : ?>
						<h4 class="green"><?php echo esc_html( $title ); ?></h4><!-- /.green -->
					<?php endif; ?>
				</div><!-- /.col-md-4 -->

				<div class="col-md-5 text-left">
					<?php echo wpautop( $text ); ?>
				</div><!-- /.col-md-5 -->
			</div><!-- /.row -->
		</div><!-- /.section__head -->

		<div class="section__foot section__foot--icon">
			<span></span>

			<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/ico-circle-leaf.svg" alt="">

			<span></span>
		</div><!-- /.section__foot -->
	</div><!-- /.container container-/-large -->
</section><!-- /.section-story -->