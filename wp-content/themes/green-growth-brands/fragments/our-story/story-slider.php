<?php
$slides = get_field( 'our_story_slider_slides' );

if ( ! $slides ) {
	return;
}
?>

<section class="section-slider pt-5">
	<div class="container container--large">
		<div class="section__body">
			<div class="slider-ethos">
				<div class="slider__clip">
					<div class="slider__slides">
						<?php foreach ( $slides as $slide ) : ?>
							<div class="slider__slide">
								<div class="slider__slide-content slider__slide-content--large">
									<?php if ( $slide['title'] ) : ?>
										<h5><?php echo esc_html( $slide['title'] ); ?></h5>
									<?php endif; ?>
									
									<?php if ( $slide['text'] ) : ?>
										<h4><?php echo $slide['text'] ?></h4>
									<?php endif; ?>
								</div><!-- /.slider__slide-content -->	
							</div><!-- /.slider__slide -->
						<?php endforeach; ?>
					</div><!-- /.slider__slides -->
				</div><!-- /.slider__clip -->
			</div><!-- /.slider -->
		</div><!-- /.section__body -->
	</div><!-- /.container -->
</section><!-- /.section-slider -->