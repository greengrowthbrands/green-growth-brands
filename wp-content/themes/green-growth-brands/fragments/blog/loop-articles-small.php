<?php
$offset = isset( $offset ) ? $offset : '4';
$next_posts = isset( $next_posts ) ? $next_posts : false;

if ( ! $next_posts ) {
	$posts = get_posts( array(
		'post_type' => 'post',
		'posts_per_page' => '10',
		'orderby' => 'date',
		'order' => 'DESC',
		'offset' => $offset,
	) );
} else {
	$posts = get_posts( array(
		'post_type' => 'post',
		'posts_per_page' => '5',
		'orderby' => 'date',
		'order' => 'DESC',
		'offset' => $offset,
	) );
}

$next_posts_check = get_posts( array(
	'post_type' => 'post',
	'posts_per_page' => '5',
	'orderby' => 'date',
	'order' => 'DESC',
	'offset' => $offset + 5,
) );
?>

<section class="section section--no-title section-articles-small">
	<div class="container">
		<div class="section__content">
			<div class="articles">
				<?php foreach ( $posts as $post ) : ?>
					<div class="article article--small article--col">
						<div class="article__image">
                            <img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'article-list-small-square' ); ?>" />
                        </div><!-- /.article__image -->
					
						<div class="article__head">
							<p class="article__meta"><?php echo get_the_date( 'F j, Y', $post->ID ); ?></p><!-- /.article__meta -->
					
							<h5 class="article__title">
								<a href="<?php echo get_the_permalink( $post->ID ); ?>"><?php echo get_the_title( $post->ID ); ?></a>
							</h5><!-- /.article__title -->
						</div><!-- /.article__head -->
					
						<?php $terms = wp_get_post_terms( $post->ID, 'category' ); ?>

						<?php if ( $terms ) : $term = array_shift( $terms ); ?>
							<div class="article__actions">
								<a href="<?php echo get_term_link( $term ); ?>" class="btn-tag"><?php echo esc_html( $term->name ); ?></a>
							</div><!-- /.article__actions -->
						<?php endif; ?>
					</div><!-- /.article -->
				<?php endforeach; ?>
			</div><!-- /.articles -->
		</div><!-- /.section__content -->

		<div class="section__actions" style="padding-bottom: 100px;">
			<?php if ( $next_posts_check ) : ?>
				<a href="#" class="btn btn--primary btn--trasparent btn--large posts-load" data-offset="14" ><?php _e( 'read more news', 'crb' ); ?></a>
			<?php endif; ?>
		</div><!-- /.section__actions -->
	</div><!-- /.container container-/-small -->
</section><!-- /.section -->