<?php
$classes = [ 'intro' ];
$image_id = '';
if ( is_page_template( 'templates/news.php' ) ) {
	$post = get_posts( array(
		'posts_per_page' => 1,
		'orderby' => 'date',
		'order' => 'DESC',
	) );

	if ( ! $post ) {
		return;
	}

	$post = array_shift( $post );

	$image_id = ! has_post_thumbnail( $post->ID ) ? get_post_thumbnail_id() : get_post_thumbnail_id( $post->ID );
}
  else if (is_category() ) {
    $title = crb_get_title();
    $classes[] = 'intro--blog';
    $image_id = get_post_thumbnail_id( crb_get_page_ID_by_template( 'templates/news.php' ) );
  }
  else if (is_tax('crb_news_category') ) {
    $title = crb_get_title();
    $classes[] = 'intro--blog';
    $image_id = get_post_thumbnail_id( crb_get_page_ID_by_template( 'templates/news.php' ) );
  }
  else if ( ! is_singular() ) {
	$title = crb_get_title();
	$image_id = get_post_thumbnail_id( crb_get_page_ID_by_template( 'templates/news.php' ) );
  }
  else {
	$post = get_post( get_the_ID() );
	$classes[] = 'intro--blog';

	$image_id = ! has_post_thumbnail( $post->ID ) ? get_post_thumbnail_id( crb_get_page_ID_by_template( 'templates/news.php' ) ) : get_post_thumbnail_id( $post->ID );
}
?>

<div class="<?php echo implode( ' ', $classes ); ?>">
	<div class="container">
		<div class="intro__image" style="background-image: url(<?php echo wp_get_attachment_url( $image_id ); ?>);"></div><!-- /.intro__image -->
		
		<?php if ( ! empty( $post ) ) : ?>
			<div class="intro__content">
				<?php if ( ! is_page_template( 'templates/news.php' ) ) : ?>
					<p class="intro__meta"><?php echo get_the_date( 'F j, Y', get_the_ID() ); ?></p><!-- /.intro__meta -->
				<?php endif; ?>

				<h2><?php echo esc_html( $post->post_title ); ?></h2>

				<?php if ( is_page_template( 'templates/news.php' ) ) : ?>
					<a href="<?php echo esc_url( get_the_permalink( $post->ID ) ); ?>" class="btn btn--primary btn--transparent btn--white"><?php _e( 'read more', 'crb' ); ?></a>
				<?php else : ?>
					<?php $terms = wp_get_post_terms( $post->ID, 'category' ); ?>

					<?php if ( $terms ) : $term = array_shift( $terms ); ?>
						<a href="<?php echo get_term_link( $term ); ?>" class="btn-tag"><?php echo esc_html( $term->name ); ?></a>
					<?php endif; ?>
				<?php endif; ?>
			</div><!-- /.intro__content -->
		<?php else : ?>
			<div class="intro__content">
				<h2><?php echo $title; ?></h2>
                <?php if(is_category()):
                    echo category_description();
                endif;?>
                <?php if(is_tax('crb_news_category')):
                    echo term_description();
                endif;?>
			</div><!-- /.intro__content -->
		<?php endif; ?>
	</div><!-- /.container -->
</div><!-- /.intro -->

<?php wp_reset_postdata(); ?>