<?php
if ( ! isset( $offset ) ) {
	$offset = '0';
}

$news_posts = get_posts( array(
	'post_type' => 'crb_news',
	'posts_per_page' => 4,
	'orderby' => 'date',
	'order' => 'DESC',
	'offset' => $offset
) );

$news_offset_posts = get_posts( array(
	'post_type' => 'crb_news',
	'posts_per_page' => 4,
	'orderby' => 'date',
	'order' => 'DESC',
	'offset' => $offset + 4
) );

if ( ! $news_posts ) {
	return;
}
?>

<section class="section section-in-the-news">
	<div class="container container--small">
		<div class="section__head">
			<h2><?php _e( 'In the News', 'crb' ); ?></h2>
		</div><!-- /.section__head -->

		<div class="section__content">
			<div class="articles articles--big">
				<?php foreach ( $news_posts as $news_post ) : ?>
					<div class="article article--small">
						<div class="article__image">
                            <img src="<?php echo get_the_post_thumbnail_url( $news_post->ID, 'large' ) ?>" />
                        </div><!-- /.article__image -->

						<div class="article__head">
							<p class="article__meta"><?php echo get_the_date( 'F j, Y', $news_post->ID ); ?></p><!-- /.article__meta -->

							<h5 class="article__title">
								<a href="<?php echo esc_url( get_field( 'link', $news_post->ID ) ); ?>" target="_blank"><?php echo esc_html( $news_post->post_title ); ?></a>
							</h5><!-- /.article__title -->
						</div><!-- /.article__head -->

						<?php $terms = wp_get_post_terms( $news_post->ID, 'crb_news_category' ); ?>

						<?php if ( $terms ) : $term = array_shift( $terms ); ?>
							<div class="article__actions">
								<a href="<?php echo get_term_link( $term ); ?>" class="btn-tag"><?php echo esc_html( $term->name ); ?></a>
							</div><!-- /.article__actions -->
						<?php endif; ?>
					</div><!-- /.article -->
				<?php endforeach; ?>
			</div><!-- /.articles -->
		</div><!-- /.section__content -->

		<?php if ( $news_offset_posts ) : ?>
			<div class="section__actions">
				<a class="btn btn--primary btn--trasparent btn--large news-load" data-fragment="loop-news" data-offset="4" href="#"><?php _e( 'more from the news room', 'crb' ); ?></a>
			</div><!-- /.section__actions -->
		<?php endif; ?>
	</div><!-- /.container container-/-small -->
</section><!-- /.section -->

<?php wp_reset_postdata(); ?>