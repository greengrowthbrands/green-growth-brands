<?php
$posts_articles_big = get_posts( array(
	'posts_per_page' => 2,
	'orderby' => 'date',
	'order' => 'DESC',
	'offset' => '2'
) );

if ( ! $posts_articles_big ) {
	return;
}
?>

<section class="section">
	<div class="container container--large">
		<?php foreach ( $posts_articles_big as $article ) : ?>
			<article class="article">
				<div class="row align-items-center justify-content-between">
					<div class="col-md-6">
						<?php if ( has_post_thumbnail( $article->ID ) ) : ?>
							<div class="article__image">
                                <?php echo get_the_post_thumbnail( $article->ID, 'big' ); ?>
							</div><!-- /.article__image -->
						<?php endif; ?>
					</div><!-- /.col-md-6 -->

					<div class="col-md-5">
						<div class="article__body">
							<div class="article__head">
								<p class="article__meta"><?php echo get_the_date( 'F j, Y', $article->ID ); ?></p><!-- /.article__meta -->

								<h4 class="article__title">
									<a href="<?php echo get_the_permalink( $article->ID ); ?>"><?php echo get_the_title( $article->ID ); ?></a>
								</h4><!-- /.article__title -->
							</div><!-- /.article__head -->

							<div class="article__content">
								<?php $content = get_extended( $article->post_content ); echo wpautop( $content['main'] ); ?>
							</div><!-- /.article__content -->

							<?php $terms = wp_get_post_terms( $article->ID, 'category' ); ?>

							<?php if ( $terms ) : $term = array_shift( $terms ); ?>
								<div class="article__actions">
									<a href="<?php echo get_term_link( $term ); ?>" class="btn-tag"><?php echo esc_html( $term->name ); ?></a>
								</div><!-- /.article__actions -->
							<?php endif; ?>
						</div><!-- /.article__body -->
					</div><!-- /.col-md-5 -->
				</div><!-- /.row -->
			</article><!-- /.article -->
		<?php endforeach; ?>
	</div><!-- /.container container-/-large -->
</section><!-- /.section -->

<?php wp_reset_postdata(); ?>