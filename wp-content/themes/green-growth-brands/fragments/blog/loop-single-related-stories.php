<?php
$posts = get_field( 'related_posts' );

if ( ! $posts ) {
	return;
}
?>

<section class="section section--borders section--related-stories">
	<div class="container container--small">
		<div class="section__head section__head--borders">
			<p><?php _e( 'Related Stories', 'crb' ); ?></p>
		</div><!-- /.section__head -->

		<div class="section__content">
			<div class="row justify-content-between">
				<?php foreach ( $posts as $post ) : ?>
					<div class="article article--small">
						<div class="article__image" style="background-image: url(<?php echo get_the_post_thumbnail_url( $post->ID ); ?>);"></div><!-- /.article__image -->

						<div class="article__head">
							<p class="article__meta"><?php echo get_the_date( 'F j, Y', $post->ID ); ?></p><!-- /.article__meta -->

							<h5 class="article__title">
								<a href="<?php echo get_the_permalink(); ?>"><?php echo esc_html( $post->post_title ); ?></a>
							</h5><!-- /.article__title -->
						</div><!-- /.article__head -->

						<?php $terms = wp_get_post_terms( $post->ID, 'category' ); ?>

						<?php if ( $terms ) : $term = array_shift( $terms ); ?>
							<div class="article__actions">
								<a href="<?php echo get_term_link( $term ); ?>" class="btn-tag"><?php echo esc_html( $term->name ); ?></a>
							</div><!-- /.article__actions -->
						<?php endif; ?>
					</div><!-- /.article -->
				<?php endforeach; ?>
			</div><!-- /.row -->
		</div><!-- /.section__content -->
	</div><!-- /.container container-/-small -->
</section><!-- /.section -->