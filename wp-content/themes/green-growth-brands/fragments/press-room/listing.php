<?php
$offset = isset( $offset ) ? $offset : '0';
$next_posts = isset( $next_posts ) ? $next_posts : false;

if ( ! $next_posts ) {
	$releases = get_posts( array(
		'post_type' => 'crb_press_release',
		'posts_per_page' => 8,
		'orderby' => 'date',
		'order' => 'DESC',
		'offset' => $offset,
	) );
} else {
	$releases = get_posts( array(
		'post_type' => 'crb_press_release',
		'posts_per_page' => 8,
		'orderby' => 'date',
		'order' => 'DESC',
		'offset' => $offset,
	) );
}

$next_posts_check = get_posts( array(
	'post_type' => 'crb_press_release',
	'posts_per_page' => 8,
	'orderby' => 'date',
	'order' => 'DESC',
	'offset' => $offset + 8,
) );

if ( ! $releases ) {
	return;
}
?>

<section class="section-press-releases section-press-releases--inner">
	<div class="container">
		<div class="section__body">
			<div class="releases">
				<?php foreach ( $releases as $release ) : ?>
					<div class="release">
						<div class="release__head">
							<p class="release__meta"><?php echo get_the_date( 'F j, Y', $release->ID ); ?></p><!-- /.article__meta -->

							<h4 class="release__title">
								<a href="<?php echo get_the_permalink( $release->ID ); ?>"><?php echo get_the_title( $release->ID ); ?></a>
							</h4><!-- /.release__title -->
						</div><!-- /.release__head -->

						<div class="release__body">
							<?php $content = get_extended( $release->post_content ); echo wpautop( $content['main'] ); ?>
						</div><!-- /.release__body -->
					</div><!-- /.release -->
				<?php endforeach; ?>
			</div><!-- /.releases -->
		</div><!-- /.section__body -->

		<div class="section__actions">
			<?php if ( $next_posts_check ) : ?>
				<a href="#" class="btn btn--primary press-release-load" data-offset="8"><?php _e( 'Load More', 'crb' ); ?></a>
			<?php endif; ?>
		</div><!-- /.section__actions -->

		<div class="section__foot section__foot--icon">
			<span></span>

			<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/ico-circle-leaf.svg" alt="">

			<span></span>
		</div><!-- /.section__foot -->
	</div><!-- /.container container-/-small -->
</section><!-- /.section-press-releases -->