<?php

# Custom hierarchical taxonomy (like categories)
register_taxonomy(
	'crb_news_category', # Taxonomy name
	array( 'crb_news' ), # Post Types
	array( # Arguments
		'labels'            => array(
			'name'              => __( 'News Categories', 'crb' ),
			'singular_name'     => __( 'News Category', 'crb' ),
			'search_items'      => __( 'Search News Categories', 'crb' ),
			'all_items'         => __( 'All News Categories', 'crb' ),
			'parent_item'       => __( 'Parent News Category', 'crb' ),
			'parent_item_colon' => __( 'Parent News Category:', 'crb' ),
			'view_item'         => __( 'View News Category', 'crb' ),
			'edit_item'         => __( 'Edit News Category', 'crb' ),
			'update_item'       => __( 'Update News Category', 'crb' ),
			'add_new_item'      => __( 'Add New News Category', 'crb' ),
			'new_item_name'     => __( 'New News Category Name', 'crb' ),
			'menu_name'         => __( 'News Categories', 'crb' ),
		),
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'news-category' ),
	)
);