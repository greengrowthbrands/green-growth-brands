<?php
get_header();

crb_render_fragment( 'blog/intro' ); ?>

<div class="main main--gray">
	<?php
	if ( is_single() ) {
	get_template_part( 'loop', 'single' );
	} else {
		get_template_part( 'loop' );
	}
	?>
</div><!-- /.main main-/-gray -->

<?php get_footer(); ?>