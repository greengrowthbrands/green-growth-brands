<?php
/*
Template Name: News
*/

get_header();

the_post();
?>

<div class="main main--gray">
	<?php
	crb_render_fragment( 'blog/intro' );

	crb_render_fragment( 'blog/loop-second-post' );

	crb_render_fragment( 'blog/loop-news' );

	crb_render_fragment( 'blog/loop-articles-big' );

	crb_render_fragment( 'blog/loop-articles-small' );

	crb_render_fragment( 'common/in-the-news-slider' );

	crb_render_fragment( 'common/subscribe' );

	crb_render_fragment( 'common/instafeed' );
	?>
</div><!-- /.main main-/-gray -->

<?php get_footer(); ?>
