<?php
/*
Template Name: Contact
*/

get_header();

the_post();

crb_render_fragment( 'common/intro' );
?>

<main class="main main--gray">
	<div class="main__outer">
		<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/single-leaf-2.svg" alt="" class="main__image main__image--2">
	</div><!-- /.main__outer -->

	<?php
	crb_render_fragment( 'common/contact' );

	get_field( 'contacts_page_show_in_the_news_slider' )  == true ? crb_render_fragment( 'common/in-the-news-slider' ) : '';

	get_field( 'contacts_page_show_subscribe_section' )  == true ? crb_render_fragment( 'common/subscribe' ) : '';

	crb_render_fragment( 'common/instafeed' );
	?>
</main><!-- /.main -->

<?php get_footer(); ?>