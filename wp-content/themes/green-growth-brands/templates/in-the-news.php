<?php
/*
Template Name: In the News
*/

get_header();

the_post();

crb_render_fragment( 'common/intro' );
?>

<div class="main main--gray">
	<?php
	crb_render_fragment( 'investors/navigation' );

	crb_render_fragment( 'in-the-news/news-big' );

	crb_render_fragment( 'in-the-news/news-small' );

	crb_render_fragment( 'in-the-news/news-archive' );

	crb_render_fragment( 'common/contact' );

	get_field( 'in_the_news_show_news_slider' )  == true ? crb_render_fragment( 'common/in-the-news-slider' ) : '';

	get_field( 'in_the_news_show_subscribe_section' )  == true ? crb_render_fragment( 'common/subscribe' ) : '';
	?>
</div>

<?php get_footer(); ?>