<?php
/*
Template Name: Press Room
*/

get_header();

the_post();

crb_render_fragment( 'common/intro' );
?>


<div class="main main--gray">
	<?php
	crb_render_fragment( 'investors/navigation' );

	crb_render_fragment( 'press-room/listing' );

	crb_render_fragment( 'common/contact' );

	get_field( 'press_room_show_in_the_news_slider' )  == true ? crb_render_fragment( 'common/in-the-news-slider' ) : '';

	get_field( 'press_room_show_subscribe_section' )  == true ? crb_render_fragment( 'common/subscribe' ) : '';
	?>

</div><!-- /.main main-/-gray -->

<?php get_footer(); ?>