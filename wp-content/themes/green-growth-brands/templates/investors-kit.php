<?php
/*
Template Name: Investors Kit
*/

get_header();

the_post();

crb_render_fragment( 'common/intro' );
?>

<div class="main main--gray">
	<?php crb_render_fragment( 'investors/navigation' ); ?>

	<section class="section-download">
		<div class="container container--small">
			<div class="row">
				<div class="col-md-6">
					<?php if ( $content = get_field( 'investors_kit_content' ) ) : ?>
						<div class="section__head">
							<?php echo crb_content( $content ); ?>
						</div><!-- /.section__head -->
					<?php endif; ?>

					<?php if ( $image = get_field( 'investors_kit_image' ) ) : ?>
						<div class="section__image">
							<?php echo wp_get_attachment_image( $image['id'], 'big' ); ?>
						</div><!-- /.section__image -->
					<?php endif; ?>
				</div><!-- /.col-md-6 -->

				<div class="col-md-5">
					<?php if ( $form = get_field( 'investors_kit_form' ) ) : ?>
						<div class="form">
							<div class="form__head">
								<h5><?php _e( 'Please complete the form below.', 'crb' ); ?></h5>
							</div><!-- /.form__head -->

							<div class="form__content">
								<?php echo do_shortcode( $form ); ?>
							</div><!-- /.form__content -->
						</div><!-- /.form -->
					<?php endif; ?>
				</div><!-- /.col-md-5 -->
			</div><!-- /.justify-content-center align-items-center -->
		</div><!-- /.container-/-small -->
	</section><!-- /.section-download -->

	<?php
	get_field( 'investors_kit_in_the_news_slider' )  == true ? crb_render_fragment( 'common/in-the-news-slider' ) : '';

	get_field( 'investors_kit_show_subscribe_section' )  == true ? crb_render_fragment( 'common/subscribe' ) : '';
	?>
</div><!-- /.main main-/-gray -->

<?php get_footer(); ?>