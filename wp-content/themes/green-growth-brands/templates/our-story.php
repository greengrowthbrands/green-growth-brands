<?php
/*
Template Name: Our Story
*/

get_header();

the_post();

crb_render_fragment( 'common/intro' );
?>

<main class="main main--gray">
	<div class="main__outer">
		<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/single-leaf-2.svg" alt="" class="main__image main__image--1 main__image--right main__image--top">
	</div><!-- /.main__outer -->

	<?php
	crb_render_fragment( 'our-story/text-columns' );

	crb_render_fragment( 'our-story/story-slider' );	

	crb_render_fragment( 'our-story/section-brands' );

	get_field( 'our_story_show_in_the_news_slider' )  == true ? crb_render_fragment( 'common/in-the-news-slider' ) : '';

	get_field( 'our_story_show_subscribe_section' )  == true ? crb_render_fragment( 'common/subscribe' ) : '';

	crb_render_fragment( 'common/instafeed' );
	?>
</main><!-- /.main -->

<?php get_footer(); ?>