<?php
/*
Template Name: Our Team
Static: Yes
*/

get_header();
?>

<div class="intro">
	<div class="container">
		<div class="intro__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/intro-image-2.png);">
			
		</div><!-- /.intro__image -->
		
		<div class="intro__content">
			<h1>Our Team.</h1>
				
			<p>We are a host of collaborative voices adding to the whole picture, and building the culture we’ve always wanted, but never had.</p>
		</div><!-- /.intro__content -->
	</div><!-- /.container -->
</div><!-- /.intro -->

<main class="main main--gray">
	<section class="section-team">
		<div class="section__outer">
			<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/ico-leaf-large.svg" alt="" class="section__outer--image section__outer--image-1">

			<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/ico-leaf.svg" alt="" class="section__outer--image section__outer--image-2">
		</div><!-- /.section__outer -->

		<header class="section__head">
			<h3>Executive Team</h3>
		</header><!-- /.section__head -->
			
		<div class="section__body">
			<div class="container container--small">
				<div class="tabs-team">
					<div class="tabs__head">
						<nav class="tabs__nav">
							<ul>
								<li class="current">
									<a id="0">
										Peter Horvath
									</a>
								</li>

								<li>
									<a id="1">
										Ian Fodie
									</a>
								</li>

								<li>
									<a id="2">
										Scott Razek
									</a>
								</li>

								<li>
									<a id="3">
										Ed Kistner
									</a>
								</li>

                                <li>
									<a id="4">
										Kellie Wurtzman
									</a>
								</li>
							</ul>
						</nav><!-- /.tabs__nav -->

						<span class="target"></span>
					</div><!-- /.tabs__head -->
					
					<div class="tabs__body tabs__body--large">
						<div class="member-entry">
							<div class="member member--team">
								<div class="row justify-content-between align-items-start">
									<div class="col-md-5">
										<div class="member__entry">
											<h4>
												Peter Horvath
											</h4>
											
											<h6>
												CEO
											</h6>
											
											<p>Peter is a dynamic creative brand leader, team builder and specialty retail veteran with deep roots in finance. Serving in critical C-level leadership roles for brands like Victoria’s Secret, American Eagle Outfitters, DSW and Limited Brands, he led strategy and execution across all company channels. Under his leadership, shoe retailer DSW went public on the NYSE at $1.5B. </p>
											
											<p>Peter’s unique ability to understand the big picture while never missing the subtle details is a critical factor in Green Growth Brands success as a company and our brands' popularity with customers. </p>
										</div><!-- /.member__content -->
									</div><!-- /.col-md-5 -->
									
									<div class="col-md-5">
										<div class="member__image">
											<img src="<?php echo get_template_directory_uri() ?>/resources/images/temp/peter-horvath-crop.jpg" alt="">
										</div><!-- /.member__image -->
									</div><!-- /.col-md-4 -->
								</div><!-- /.row -->

							</div>
						</div><!-- /.member-entry" -->

						<div class="member-entry">
							<div class="member member--team">
								<div class="row justify-content-between align-items-start">
									<div class="col-md-5">
										<div class="member__entry">
											<h4>
												Ian Fodie
											</h4>
											
											<h6>
												CFO
											</h6>
											
											<p>
												This numbers guy comes with a wealth of experiences dealing in financial matters for fast-paced, high energy industries of entertainment, investment and mining exploration and development, including operational responsibilities and public accounting experience with Deloitte in Canada and New Zealand, over a career spanning 25 years. These industries share the challenges of this emerging business, such as complex financing and reporting, and changing regulatory environments, where Ian has also provided strategic and corporate governance expertise
											</p>
											
											<p>
												He has been instrumental in leading and negotiating financing transactions in public markets, private placements and debt financings. Ian is a New Zealand-certified Chartered Accountant (similar to CPA certification in the US) who brings commitment and creativity to everything that he undertakes to build efficient and effective teams to drive business and strategic objectives for the benefits of all stakeholders.
											</p>
										</div><!-- /.member__content -->
									</div><!-- /.col-md-5 -->
									
									<div class="col-md-5">
										<div class="member__image">
											<img src="<?php echo get_template_directory_uri() ?>/resources/images/temp/ian-fodie-crop.jpg" alt="">
										</div><!-- /.member__image -->
									</div><!-- /.col-md-4 -->
								</div><!-- /.row -->

							</div>
						</div><!-- /.member-entry" -->

						<div class="member-entry">
							<div class="member member--team">
								<div class="row justify-content-between align-items-start">
									<div class="col-md-5">
										<div class="member__entry">
											<h4>
												Scott Razek
											</h4>
											
											<h6>
												CMO
											</h6>
											
											<p>
												A Brand Strategist, Storyteller and strategic Marketer, Scott spent 25 years of his career leading creative teams and marketing efforts for retail brands such as Victoria’s Secret, Bath & Body Works, Limited Stores and American Eagle Outfitters. His guiding philosophy is that great brands are well defined: Consistent, clear, relevant, differentiated. To the company and to the consumer.
											</p>
											
											<p>
												He is clear, he is bold and he keeps the customers at the center of every decision. His experience in brand building, product development and customer experience focus are a key differentiator for the Green Growth Brands portfolio.  In a nutshell, his entire professional life has brought him to this brand, at this time, for this purpose.
											</p>
										</div><!-- /.member__content -->
									</div><!-- /.col-md-5 -->
									
									<div class="col-md-5">
										<div class="member__image">
											<img src="<?php echo get_template_directory_uri() ?>/resources/images/temp/scott-razek-crop.jpg" alt="">
										</div><!-- /.member__image -->
									</div><!-- /.col-md-4 -->
								</div><!-- /.row -->

							</div>	
						</div><!-- /.member-entry" -->

						<div class="member-entry">
							<div class="member member--team">
								<div class="row justify-content-between align-items-start">
									<div class="col-md-5">
										<div class="member__entry">
											<h4>
												Ed Kistner
											</h4>
											
											<h6>
												CAO
											</h6>
											
											<p>
												Ed and Peter work as the right and left hands of a single leader. Their philosophies and methodologies are completely in-sync and aligned.  This efficiency at the top facilitates rapid change and growth and the ability to pivot with efficiency, focus and purpose. Ed brings 33 years of multi-faceted experience at leading retail businesses—including leadership roles at DSW and L Brands (Victoria’s Secret)—in finance, merchandise planning, operations and stores. His well-rounded experiences in fast-changing environments, position him to be the architect of the operational execution at Green Growth Brands.
											</p>
										</div><!-- /.member__content -->
									</div><!-- /.col-md-5 -->
									
									<div class="col-md-5">
										<div class="member__image">
											<img src="<?php echo get_template_directory_uri() ?>/resources/images/temp/ed-kistner-crop.jpg" alt="">
										</div><!-- /.member__image -->
									</div><!-- /.col-md-4 -->
								</div><!-- /.row -->

							</div>	
						</div><!-- /.member-entry" -->

                        <div class="member-entry">
							<div class="member member--team">
								<div class="row justify-content-between align-items-start">
									<div class="col-md-5">
										<div class="member__entry">
											<h4>
												Kellie Wurtzman
											</h4>

											<h6>
												CSO
											</h6>

											<p>
												Kellie brings significant retail leadership to Green Growth Brands with a proven track record of leading high-performance stores and teams at brands like Luxottica, Victoria’s Secret and Virgin Entertainment. She brings a diverse blend of office, field and analytical experience that gives her a unique ability to solve complex problems with innovative, customer-focused solutions.
											</p>

                                            <p>
												Kellie has managed operations across multiple retail sectors, giving her skills that perfectly translate to growing operations in a newly emerging industry. Her unmatched experience in identifying and supporting developing business opportunities is ideal for evolving the cannabis industry and will be instrumental in expanding our operations.
											</p>
										</div><!-- /.member__content -->
									</div><!-- /.col-md-5 -->

									<div class="col-md-5">
										<div class="member__image">
											<img src="<?php echo get_template_directory_uri() ?>/resources/images/temp/kellie-wurtzman-crop.jpg" alt="">
										</div><!-- /.member__image -->
									</div><!-- /.col-md-4 -->
								</div><!-- /.row -->

							</div>
						</div><!-- /.member-entry" -->
					</div><!-- /.tabs__body -->
				</div><!-- /.tabs -->
			</div><!-- /.container -->
		</div><!-- /.section__body -->
	</section><!-- /.section-brands -->



	<section id="careers" class="section-brands">
		<div class="container container--small">
			<div class="section__head section__head--large">
				<h2 class="section__title">Careers</h2><!-- /.section__title -->

				<div class="row justify-content-between align-items-center">
					<div class="col-md-5 text-left">
						<h4 class="green">For us, it’s more than a job. It’s a movement. If you’re hearing us, you’re probably one of us.</h4><!-- /.green -->
					</div><!-- /.col-md-4 -->

					<div class="col-md-6 text-left">
						<p>GGB offers an opportunity that’s unique: A chance to bring your individual voice to a global effort. A chance to help define an industry. We’re doing something that’s never been done before. Join us.</p>
					</div><!-- /.col-md-5 -->
                    <!--<div class="col-md-12" style="margin-top: 40px;">
                        <a href="http://greengrowthbrandsllc.appone.com/" target="_blank" class="btn btn--primary">
                            Available Positions
                        </a>
                    </div>
                    -->
				</div><!-- /.row -->
			</div><!-- /.section__head -->

			<div class="section__body">
				<div class="accordions">
					<div class="accordions__head">
						<h3 class="green">Ohio</h3><!-- /.green -->
					</div><!-- /.accordions__head -->

					<div class="accordions__body">
						<div class="accordion">
							<div class="accordion__section">
								<div class="accordion__head">
									<i class="ico-plus">
										<span></span>

										<span></span>
									</i>

									<h6 class="green">Paralegal</h6><!-- /.green -->

									<p>Columbus, Ohio</p>
								</div><!-- /.accordion__head -->

								<div class="accordion__body">
									<h6>What you'll do:</h6>
                                    <ul>
                                        <li>Manage corporate contracts database and template library, updating as needed.</li>
                                        <li>
                                            Support corporate projects, including due diligence, and drafting basic legal documents such as nondisclosure agreements.
                                        </li>
                                        <li>Work with members of the applicable business teams to manage contract deliverables and assist with special projects and enterprise-wide initiatives.</li>
                                        <li>Assist with contract review and creation of templates.</li>
                                        <li>Support and develop processes for third party vendor management.</li>
                                        <li>Assist with PowerPoint presentations.</li>
                                        <li>Help drive key Legal team initiatives.</li>
                                        <li>Coordinate with external/outside counsel.</li>
                                    </ul>
                                    <h6>We've Been Looking for You.</h6>
                                    <ul>
                                        <li>You have experience drafting, reviewing and revising contracts, such as purchase agreements, software licenses, supply agreements, government contracts, services agreements, non-disclosure agreements, or memorandums of understanding.
                                        </li>
                                        <li>
                                You have experience in a role that required strong written and verbal communication skills. Excellent communication skills, both oral and written, to explain legal positions to others, including attorneys and paralegals representing Green Growth Brands.                                               </li>
                                        <li>Strong organizational, problem-solving and analytical skills.</li>
                                        <li>Previous experience in role requiring high level of integrity and discretion in handling confidential information.</li>
                                        <li>Proven ability to handle multiple projects, prioritize and meet deadlines.</li>
                                        <li>Excellent judgment with the ability to problem solve and make timely/sound decisions.</li>
                                        <li>You have 3-5 years of experience as a paralegal in a corporate setting; public company experience a plus.</li>
                                    </ul>
                                    <p>To apply, please email your resume to <a href="mailto:careers@greengrowthbrands.com" target="_blank">careers@greengrowthbrands.com</a> and include a writing sample, not to exceed 3 pages, with your submission.

</p>
								</div><!-- /.accordion__body -->
							</div><!-- /.accordion__section -->
						</div><!-- /.accrodion -->

					</div><!-- /.accordions__body -->
				</div><!-- /.accordions -->

			</div><!-- /.section__body -->
		</div><!-- /.container -->
	</section><!-- /.section-brands -->

    <?php

    crb_render_fragment( 'common/in-the-news-slider' );

	crb_render_fragment( 'common/subscribe' );

	crb_render_fragment( 'common/instafeed' );

    ?>
	

</main><!-- /.main -->

<?php get_footer(); ?>