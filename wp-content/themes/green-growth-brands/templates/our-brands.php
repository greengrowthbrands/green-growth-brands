<?php
/*
Template Name: Our Brands
Static: Yes
*/

get_header();
?>

<div class="intro">
	<div class="container">
		<div class="intro__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/intro-image-1.png);">
			
		</div><!-- /.intro__image -->
		
		<div class="intro__content">
			<h1>
				Our Brands.
			</h1>
				
			<p>
				Each of our brands serves a distinct market. But all of them are known for providing remarkable products and unmatched experiences.
			</p>
		</div><!-- /.intro__content -->
	</div><!-- /.container -->
</div><!-- /.intro -->

<main class="main main--gray">
	<div class="brands-wrapper">
		<section class="section-camp section--brands">
			<div class="container">
				<div class="row align-items-start">
					<div class="col-md-6">
						<div class="section__content">
							<div class="section__icon">
								<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/brand-camp.svg" alt="" class="logo-camp-brand">
							</div><!-- /.section__icon -->
							
							<div class="section__entry">
								<h4>
									More than a place to gather; it’s a way of being.
								</h4>

								<p>
									Be one with nature. Find your center. Meet your tribe. This is CAMP: A store for the cannabis community to find premium products that support and enhance your active lifestyle. It’s more than a place to gather, it’s a movement, a way of being. Start your journey to self-discovery with a visit to CAMP and discover your new happy place.
								</p>
							</div><!-- /.section__entry -->
							
							<div class="section__actions">
								<!--<a href="#" class="btn btn--primary btn--transparent">
									Shop now
								</a>-->
							</div><!-- /.section__actions -->
						</div><!-- /.section__content -->
					</div><!-- /.col-md-6 -->
					
					<div class="col-md-6">
						<aside class="section__aside">
							<div class="section__image-grid" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image.png);" data-aos="fade-left">

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-2.png);" data-aos="fade-left">
								</div><!-- /.section__image -->

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-3.png); " data-aos="fade-left">
								</div><!-- /.section__image -->
							</div><!-- /.section__image -->
						</aside><!-- /.section__aside -->
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.section-camp -->
		
		<section class="section-brands-left section--brands">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<aside class="section__aside">
							<div class="section__image-grid" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid.png);" data-aos="fade-right">

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-2.png);" data-aos="fade-right">
								</div><!-- /.section__image -->

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-3.png);" data-aos="fade-right">
								</div><!-- /.section__image -->
							</div><!-- /.section__image -->
						</aside><!-- /.section__aside -->
					</div><!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="section__content">
							<div class="section__icon">
								<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/brand-7th-sense.svg" alt="" class="logo-7th-brand">
							</div><!-- /.section__icon -->
							
							<div class="section__entry">
								<h4>
									Awaken your body’s natural healing system.
								</h4>
								
								<p>
									This line of beautiful, efficacious, CBD infused beauty products will soon be available nationally. From body wash, lotions and balms, to hair care, lip balm and sun products, Seventh Sense is meant to awaken your body’s natural healing system to promote calm, better sleep, happiness and health.
								</p>
							</div><!-- /.section__entry -->
							
							<div class="section__actions">
								<a href="http://shop7thsense.com/" target="_blank" class="btn btn--primary">
									Shop Now
								</a>
							</div><!-- /.section__actions -->
						</div><!-- /.section__content -->
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.conrainer -->
		</section><!-- /.section-brands-left -->

		<section class="section-brands-right section--brands">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="section__content">
							<div class="section__icon">
								<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/brand-meri-jayne.svg" alt="" class="logo-meri-brand">
							</div><!-- /.section__icon -->
							
							<div class="section__entry">
								<h4>
									Where good times and good health meet.
								</h4>
								
								<p>
									Be your bold, carefree self. At Meri+Jayne, we craft and curate an unexpected mix of cannabis products that lets you do you. This is where good times and good health meet, where you can come to let loose and find new ways to make every moment more fun. Need a tip or two? Our friendly staff knows what’s up. Come check it out. Do what you feel. #ChillAtWill
								</p>
							</div><!-- /.section__entry -->
							
							<div class="section__actions">
								<!--<a href="#" class="btn btn--primary">
									Learn More
								</a>-->
							</div><!-- /.section__actions -->
						</div><!-- /.section__content -->
					</div><!-- /.col-md-6 -->

					<div class="col-md-6">
						<aside class="section__aside">
							<div class="section__image-grid" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-right.png);" data-aos="fade-left">

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-right-2.png);" data-aos="fade-left">
								</div><!-- /.section__image -->

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-right-3.png);" data-aos="fade-left">
								</div><!-- /.section__image -->
							</div><!-- /.section__image -->
						</aside><!-- /.section__aside -->
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.conrainer -->
		</section><!-- /.section-brands-right -->

		<section class="section-brands-left section-brands-left--small-gutters section--brands">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<aside class="section__aside">
							<div class="section__image-grid" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-left.png);" data-aos="fade-right">

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-left-2.png);" data-aos="fade-right">
								</div><!-- /.section__image -->

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-left-3.png);" data-aos="fade-right">
								</div><!-- /.section__image -->
							</div><!-- /.section__image -->
						</aside><!-- /.section__aside -->
					</div><!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="section__content">
							<div class="section__icon">
								<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/brand-green-lily.svg" alt="" class="logo-lily-brand">
							</div><!-- /.section__icon -->
							
							<div class="section__entry">
								<h4>
									A new world of wellness.
								</h4>
								
								<p>
									This is a store that is designed especially for women to explore their femininity. Incredible botanically led beauty products. Beautiful, inviting, comfortable interiors. Friendly and knowledgeable guides. Green Lily is a whole new world of wellness with products that match her every need.
								</p>
							</div><!-- /.section__entry -->
							
							<div class="section__actions">
								<!--<a href="#" class="btn btn--primary">
									Learn More
								</a>-->
							</div><!-- /.section__actions -->
						</div><!-- /.section__content -->
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.conrainer -->
		</section><!-- /.section-brands-left -->


		<section class="section-brands-right section-brands-right--with-title section--brands">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="section__content">
							<div class="section__icon">
								<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/brand-the-source.svg" class="logo-source-brand" alt="">
							</div><!-- /.section__icon -->

							<div class="section__entry">
								<h4>Our favorite customer experience in Las Vegas—and beyond.</h4>
								
                                <p>Due to its meticulous layout, efficient operation and friendly, knowledgeable staff, The Source was voted “Best of the City” for Las Vegas by NPR’s Desert Companion magazine. The Source is the retail brand of Nevada Organic Remedies, LLC, a vertically integrated medical and retail marijuana company that holds four Nevada marijuana licenses, including dispensary, cultivation, production and distribution. It’s easily one of the most highly productive retail stores we’ve ever seen. More information can be found at <a href="https://thesourcenv.com/" target="_blank">www.thesourcenv.com</a>.</p>
							</div><!-- /.section__entry -->
							
							<div class="section__actions">
								<a href="https://thesourcenv.com/" target="_blank" class="btn btn--primary">
									Learn More
								</a>
							</div><!-- /.section__actions -->
						</div><!-- /.section__content -->
					</div><!-- /.col-md-6 -->

					<div class="col-md-6">
						<aside class="section__aside">
							<div class="section__image-grid" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-right-4.png);" data-aos="fade-left">

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-right-5.png);" data-aos="fade-left">
								</div><!-- /.section__image -->

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-right-6.png);" data-aos="fade-left">
								</div><!-- /.section__image -->
							</div><!-- /.section__image -->
						</aside><!-- /.section__aside -->
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.conrainer -->
		</section><!-- /.section-brands-right -->

		<section class="section-brands-left section-brands-left--with-title section--brands">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<aside class="section__aside">
							<div class="section__image-grid" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-left-4.png);" data-aos="fade-right">

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-left-5.png);" data-aos="fade-right">
								</div><!-- /.section__image -->

								<div class="section__image" style="background-image: url(<?php echo get_template_directory_uri() ?>/resources/images/temp/section-image-grid-left-6.png);" data-aos="fade-right">
								</div><!-- /.section__image -->
							</div><!-- /.section__image -->
						</aside><!-- /.section__aside -->
					</div><!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="section__content">
							<div class="section__icon">
								<img src="<?php echo get_template_directory_uri() ?>/resources/images/svg/brand-xanthic.svg" class="logo-xanthic-brand" alt="">
							</div><!-- /.section__icon -->

							<div class="section__entry">
								<h4>Unique intellectual property that drives innovation</h4>
								
								<p>Xanthic is a developer of a patent-pending proprietary process to make THC and CBD, the two key active ingredients in cannabis, water soluble. As a result of our Business Combination, GGB will be able to use this process to create innovative new products and ways to enjoy all the benefits of this powerful plant. </p>
							</div><!-- /.section__entry -->
							
							<div class="section__actions">
								<a href="https://xanthicbio.com/" target="_blank" class="btn btn--primary">
									Learn More
								</a>
							</div><!-- /.section__actions -->
						</div><!-- /.section__content -->
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</div><!-- /.conrainer -->
		</section><!-- /.section-brands-left -->
	</div><!-- /.brands-wrapper -->
	

    <?php

    crb_render_fragment( 'common/in-the-news-slider' );

	crb_render_fragment( 'common/subscribe' );

	crb_render_fragment( 'common/instafeed' );

    ?>


</main><!-- /.main -->

<?php get_footer(); ?>