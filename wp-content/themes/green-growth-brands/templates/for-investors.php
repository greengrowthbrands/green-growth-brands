<?php
/*
Template Name: For Investors
*/

get_header();

the_post();

crb_render_fragment( 'common/intro' );
?>

<div class="main main--gray">
	<?php
	crb_render_fragment( 'investors/navigation' );

	crb_render_fragment( 'investors/experience' );

	crb_render_fragment( 'investors/investors-kit' );

    crb_render_fragment( 'investors/investors-kit-download' );

	crb_render_fragment( 'investors/news' );

	crb_render_fragment( 'investors/press-releases' );

	crb_render_fragment( 'common/testimonial-slider' );

	get_field( 'for_investors_in_the_news_slider' )  == true ? crb_render_fragment( 'common/in-the-news-slider' ) : '';

	get_field( 'for_investors_show_subscribe_section' )  == true ? crb_render_fragment( 'common/subscribe' ) : '';

	crb_render_fragment( 'common/instafeed' );
	?>
</div><!-- /.main main-/-gray -->

<?php get_footer(); ?>