<?php
/*
Template Name: Home
*/

get_header();

the_post();

crb_render_fragment( 'home/intro' );
?>

<main class="main main--gray">
	<?php
	crb_render_fragment( 'home/company' );

	get_field( 'homepage_show_testimonials_slider' )  == true ? crb_render_fragment( 'common/slider-testimonials' ) : '';

	crb_render_fragment( 'home/brands' );

	crb_render_fragment( 'home/team' );

	get_field( 'homepage_show_in_the_news_slider' )  == true ? crb_render_fragment( 'common/in-the-news-slider' ) : '';

	get_field( 'homepage_show_subscribe_section' )  == true ? crb_render_fragment( 'common/subscribe' ) : '';

	crb_render_fragment( 'common/instafeed' );
	?>
</main><!-- /.main -->	

<?php get_footer(); ?>