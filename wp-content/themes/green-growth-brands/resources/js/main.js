import 'bootstrap';
import Plyr from 'plyr';

import tabs from './tabsUI';
import sliders from './sliderUI';
import toggleElem from './toggleElements';
import AOS from 'aos';

const $win = $(window);
const $doc = $(document);

sliders();
toggleElem();
tabs();
AOS.init();

$('.link-scroll').on('click', function(event) {
	event.preventDefault();

	const src = $(this).attr('href');

	$('html, body').animate(
		{
			scrollTop: $(src).offset().top
		},
		700
	);
});

// Fixed Header

var stickyOffset = $('.main').offset().top;

$win.scroll(function() {
	var sticky = $('.header'),
		scroll = $win.scrollTop();

	if (scroll > stickyOffset) {
		sticky.addClass('fixed');
		sticky.find('.logo__image').hide();
		sticky.find('.logo__image-fixed').show();
	} else {
		sticky.removeClass('fixed');
		sticky.find('.logo__image').show();
		sticky.find('.logo__image-fixed').hide();	
	}
});

// Player

const player = new Plyr('#player', {});

// Play video

$('.js-play-video').on('click', function(e) {
	e.preventDefault();
	$('.plyr').addClass('active');
	player.play();
});

// Accordions
$('.accordion__section')
	.not('.accordion__section--current')
	.find('.accordion__body')
	.hide();

$('.accordion').on('click', '.accordion__head', function(event) {
	$(this)
		.next()
		.stop()
		.slideToggle()
		.parent()
		.toggleClass('accordion__section--current');

	$(this)
		.closest('.accordion__head')
		.toggleClass('active');
});

// Load more

function fetchData() {
	var headerH = $('.header').outerHeight();

	$('.js-btn-load-more').on('click', function(event) {
		event.preventDefault();

		var $this = $(this);
		var $itemsContainer = $($this.data('container'));
		var itemsUrl = $this.attr('href');

		$.ajax({
			url: itemsUrl,
			cache: false,
			success: function(response) {
				$itemsContainer
					.hide()
					.append(response)
					.fadeIn('slow');

				$itemsContainer
					.siblings('.section__actions')
					.children('.js-btn-load-more')
					.addClass('hidden');
			}
		});
	});
}

fetchData();

$('.section__actions').on('click', '.news-load', function(e){
	e.preventDefault();
	const $this = $(this);
	const $articleContainer = $this.parents('section').find('.articles.articles--big');
	const $actionContainer = $articleContainer.parents('section').find('.section__actions');
	const offset = $this.data('offset');
	const fragment = $this.data('fragment');

	let offsetIncrement = ( fragment == 'loop-news' ? 4 : 5 ); 

	const data = {
		action: 'crb_get_news_posts',
		offset: offset,
		fragment: fragment,
	};

	$this.remove();

	$.ajax({
		type: 'post',
		url: siteData.ajaxUrl,
		data: data,
		success: (response) => {
			const $articles = $('.article--small', response);
			const $button = $('.news-load', response);
			
			$button.attr('data-offset', offset + offsetIncrement);
			$articleContainer.append( $articles );
			$actionContainer.append($button);	
		}
	})
})

$('.section__actions').on('click', '.posts-load', function(e){
	e.preventDefault();
	const $this = $(this);
	const $articleContainer = $('.section-articles-small .articles');
	const $actionContainer = $('.section-articles-small .section__actions');
	var offset = $(this).data('offset');

	const data = {
		action: 'crb_get_posts',
		offset: offset,
	};

	$this.remove();

	$.ajax({
		type: 'post',
		url: siteData.ajaxUrl,
		data: data,
		success: (response) => {
			const $articles = $('.article--small', response);
			const $button = $('.posts-load', response);
			
			$button.attr('data-offset', offset + 5);
			$articleContainer.append( $articles );
			$actionContainer.append($button);	
		}
	})
})

$('.section__actions').on('click', '.next-page-load', function(e){
	e.preventDefault();
	const $this = $(this);
	const $articleContainer = $('.section--category-archive .articles');
	const $actionContainer = $('.section--category-archive .section__actions');

	$this.remove();

	$.ajax({
		type: 'post',
		url: $this.attr('href'),
		success: (response) => {
			const $articles = $('.article--small', response);
			const $button = $('.next-page-load', response);

			$articleContainer.append( $articles );
			$actionContainer.append($button);	
		}
	})
})

$('.section__actions').on('click', '.press-release-load', function(e){
	e.preventDefault();
	const $this = $(this);
	const $articleContainer = $('.section-press-releases .releases');
	const $actionContainer = $('.section-press-releases .section__actions');
	var offset = $(this).data('offset');
	const data = {
		action: 'crb_get_press_releases',
		offset: offset,
	};

	$this.remove();

	$.ajax({
		type: 'post',
		url: siteData.ajaxUrl,
		data: data,
		success: (response) => {
			const $articles = $('.release', response);
			const $button = $('.press-release-load', response);
			
			$button.attr('data-offset', offset + 8);
			$articleContainer.append( $articles );
			$actionContainer.append($button);	
		}
	})
})