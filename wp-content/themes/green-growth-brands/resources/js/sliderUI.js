import 'slick-carousel';

const sliders = () => {
	const $win = $(window);
	let $sliderNews = $('.slider-news .slider__slides'); 
	let $sliderEthos = $('.slider-ethos .slider__slides'); 
	let $sliderPaging = $('.slider__paging li a');
	let $logoSlider = $('.slider-news .slider__paging ul');
	let $relatedStories = $('.section--related-stories .row');
	let $sliderBrands = $('.tabs-brands .tabs__body');
	let $sliderBrandsNav = $('.tabs-brands .tabs__nav');
	let $sliderTeam = $('.tabs-team .tabs__body');
	let $sliderTeamNav = $('.tabs-team .tabs__nav');
	let $sectionBrandsSlider = $('.brands-wrapper');

	if ( $win.width() < 768 ) {
		$sectionBrandsSlider.slick({
			dots: false,
			arrows: false,
		});
	}



	$sliderBrands.on('init', function(){
		const $leftImage = $sliderBrands.find('[data-slick-index="0"]').find('.brand__image-grid .brand__image:last-child');
		let contentHeight = $sliderBrands.find('[data-slick-index="0"]').find('.brand__content').outerHeight();

		$leftImage.css('top', `${contentHeight + 50}px` );
	})

	$sliderBrands.slick({
		dots: false,
		arrows: false,
		slidesToShow: 1.2,
		infinite: false,
		centerMode: true,
		variableWidth: true,
	}).on('click', function(){
		$sliderBrands.slick('slickNext');
	}).on('beforeChange', function(event, slick, currentSlide, nextSlide){
		$sliderBrandsNav.find(`#${currentSlide}`).parent('li').removeClass('current');
		$sliderBrandsNav.find(`#${nextSlide}`).parent('li').addClass('current');
		
		const $target = $('.tabs-brands .target');
		const $links = $('.tabs-brands .tabs__nav a')
		const $link =  $links.parents('.tabs__nav').find('li.current a');
		const width = $link.outerWidth();
		const height = $link.outerHeight();
		const position = $link.position();

		const $leftImage = $sliderBrands.find(`[data-slick-index=${nextSlide}]`).find('.brand__image-grid .brand__image:last-child');
		let contentHeight = $sliderBrands.find(`[data-slick-index=${nextSlide}]`).find('.brand__content').outerHeight();

		$target.addClass('transition');
		$target.css( 'width', `${width}px` );
		$target.css( 'height', `${height}px` );
		$target.css( 'left', `${position.left + 58}px` );
		$target.css( 'top', `${position.top}px` );

		$leftImage.css('top', `${contentHeight + 50}px` );
	})

	$sliderBrandsNav.on('click', 'a', function(){
		let target = $(this).attr('id');
		$sliderBrands.slick( 'slickGoTo', parseInt( target ) )
	})

	$sliderTeam.slick({
		dots: false,
		arrows: false,
		slidesToShow: 1,
		responsive: [{
			breakpoint: 768,
			settings: {
				adaptiveHeight: true
			}
		}]
	}).on('click', function(){
		$sliderTeam.slick('slickNext');
	}).on('beforeChange', function(event, slick, currentSlide, nextSlide){
		$sliderTeamNav.find(`#${currentSlide}`).parent('li').removeClass('current');
		$sliderTeamNav.find(`#${nextSlide}`).parent('li').addClass('current');
		
		const $target = $('.tabs-team .target');
		const $links = $('.tabs-team .tabs__nav a')
		const $link =  $links.parents('.tabs__nav').find('li.current a');
		const width = $link.outerWidth();
		const height = $link.outerHeight();
		const position = $link.position();

		$target.css( 'width', `${width}px` );
		$target.css( 'height', `${height}px` );
		$target.css( 'left', `${position.left + 58}px` );
		$target.css( 'top', `${position.top}px` );
	})

	$sliderTeamNav.on('click', 'a', function(){
		let target = $(this).attr('id');
		$sliderTeam.slick( 'slickGoTo', parseInt( target ) )
	})

	$sliderNews.slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000
	}).on('beforeChange', function(event, slick, currentSlide, nextSlide){
  		let elSlide = $(slick.$slides[nextSlide]);
    	let dataIndex = elSlide.data('slick-index');
		let $sliderList= $('.slider__paging li');

	    $sliderList.each(function() {
	    	var $this = $(this);
		    
		    if ($this.index() == dataIndex) {
		      	
		      	$this
		      		.addClass('current')
        			.siblings()
        			.removeClass('current');
			}
		});
	});

	$sliderPaging.on('click', function(event) {
		event.preventDefault();
		
		const $this = $(this);
		
		$this
			.parent()
			.addClass('current')
			.siblings()
			.removeClass('current');

		let index = $(this).parent().index();
		
		$sliderNews.slick('slickGoTo', parseInt(index));
	});

	$sliderEthos.slick({
		dots: true,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
	})

	if ( $win.width() < 768 ) {
		$logoSlider.slick({
			arrows: false,
			slidesToShow: 1.5,
			infinite: false,
			centerMode: true,
			variableWidth: true,
			speed: 300,
		});

		$sliderNews.on('beforeChange', function( event, slick, currentSlide, nextSlide ){
			$logoSlider.slick( 'slickGoTo', parseInt( nextSlide ) );
		})
	}

	$relatedStories.slick({
		dots: false,
		infinite: true,
		arrows: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,

	})
}

export default sliders;
