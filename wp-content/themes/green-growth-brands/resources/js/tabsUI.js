const tabs = () =>{
	if ( ! $('.target').length ) {
		return;
	}

	targetPosition();
	mobileScrollHandler();
	
	$(window).on('resize', resizeHandler);

	function resizeHandler() {
		targetPosition();
	}

	function targetPosition() {
		$('.target').each(function(){
			const $this = $(this);
			const $activeLink =  $this.parents('.section__body').find('li.current a');
			const width = $activeLink.outerWidth();
			const height = $activeLink.outerHeight();
			const position = $activeLink.position();
			
			$this.removeClass('transition');
			$this.css( 'width', `${width}px` );
			$this.css( 'height', `${height}px` );
			$this.css( 'left', `${position.left + 58}px` );
			$this.css( 'top', `${position.top}px` );
		})
	}

	function mobileScrollHandler() {
		$('.target').each(function(){
			const $this = $(this);
			const $container = $this.parents('.section__body').find('ul');

			$container.on('scroll', targetPosition );
		})
	}
}
export default tabs;