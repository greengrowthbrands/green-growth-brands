const toggleElem = () => {
	$('.nav-trigger').on('click', function(event){
	    event.preventDefault();
	    const $header = $('.header');
	    const $nav = $('.nav');
	    
	    $(this).toggleClass('nav-trigger--active');
	    
	    // $('html').toggleClass('fixed');      
	    $('body').toggleClass('fixed');      

	    $nav.toggleClass('visible');
	});
}

export default toggleElem;