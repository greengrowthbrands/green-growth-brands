<?php
define( 'CRB_THEME_DIR', dirname( __FILE__ ) . DIRECTORY_SEPARATOR );

# Enqueue JS and CSS assets on the front-end
add_action( 'wp_enqueue_scripts', 'crb_enqueue_assets' );
function crb_enqueue_assets() {
	$template_dir = get_template_directory_uri();

	# Enqueue Custom JS files
	wp_enqueue_script(
		'theme-js-bundle',
		$template_dir . crb_assets_bundle( 'js/bundle.js' ),
		array( 'jquery' ), // deps
		null, // version -- this is handled by the bundle manifest
		true // in footer
	);
	wp_enqueue_script( 'plugins', get_template_directory_uri() . '/vendor/plugins/plugins.js', array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'custom', get_template_directory_uri() . '/dist/js/custom.js', array('jquery'), '1.0.0', true );

	wp_localize_script( 'theme-js-bundle', 'siteData', [
			'ajaxUrl' => admin_url( 'admin-ajax.php' ),
			'instaUserId' => get_field( 'instagram', 'option' )['user_id'],
			'instaToken' => get_field( 'instagram', 'option' )['token'],
	] );

	# Enqueue Custom CSS files
	wp_enqueue_style(
		'theme-css-bundle',
		$template_dir . crb_assets_bundle( 'css/bundle.css' )
	);

	# Enqueue FontAwesome CSS
	wp_enqueue_style(
		'theme-fontawesome',
		'https://use.fontawesome.com/releases/v5.3.1/css/all.css'
	);

	# The theme style.css file may contain overrides for the bundled styles
	crb_enqueue_style( 'theme-styles', $template_dir . '/style.css' );

	# Enqueue Comments JS file
	if ( is_singular() ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

# Enqueue JS and CSS assets on admin pages
add_action( 'admin_enqueue_scripts', 'crb_admin_enqueue_scripts' );
function crb_admin_enqueue_scripts() {
	$template_dir = get_template_directory_uri();

	# Enqueue Scripts
	# @crb_enqueue_script attributes -- id, location, dependencies, in_footer = false
	# crb_enqueue_script( 'theme-admin-functions', $template_dir . '/js/admin-functions.js', array( 'jquery' ) );

	# Enqueue Styles
	# @crb_enqueue_style attributes -- id, location, dependencies, media = all
	# crb_enqueue_style( 'theme-admin-styles', $template_dir . '/css/admin-style.css' );
}

# Attach Custom Post Types and Custom Taxonomies
add_action( 'init', 'crb_attach_post_types_and_taxonomies', 0 );
function crb_attach_post_types_and_taxonomies() {
	# Attach Custom Post Types
	include_once( CRB_THEME_DIR . 'options/post-types.php' );

	# Attach Custom Taxonomies
	include_once( CRB_THEME_DIR . 'options/taxonomies.php' );
}

add_action( 'after_setup_theme', 'crb_setup_theme' );

# To override theme setup process in a child theme, add your own crb_setup_theme() to your child theme's
# functions.php file.
if ( ! function_exists( 'crb_setup_theme' ) ) {
	function crb_setup_theme() {
		# Make this theme available for translation.
		load_theme_textdomain( 'crb', get_template_directory() . '/languages' );

		# Autoload dependencies
		$autoload_dir = CRB_THEME_DIR . 'vendor/autoload.php';
		if ( ! is_readable( $autoload_dir ) ) {
			wp_die( __( 'Please, run <code>composer install</code> to download and install the theme dependencies.', 'crb' ) );
		}
		include_once( $autoload_dir );
		\Carbon_Fields\Carbon_Fields::boot();

		# Additional libraries and includes
		include_once( CRB_THEME_DIR . 'includes/admin-login.php' );
		include_once( CRB_THEME_DIR . 'includes/comments.php' );
		include_once( CRB_THEME_DIR . 'includes/title.php' );
		include_once( CRB_THEME_DIR . 'includes/gravity-forms.php' );
		include_once( CRB_THEME_DIR . 'includes/custom-fields-functions.php' );
		include_once( CRB_THEME_DIR . 'includes/ajax-pagination.php' );

		# Theme supports
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'gallery' ) );

		# Manually select Post Formats to be supported - http://codex.wordpress.org/Post_Formats
		// add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );

		# Register Theme Menu Locations
		register_nav_menus( array(
			'main-menu-left' => __( 'Main Menu Left', 'crb' ),
			'main-menu-right' => __( 'Main Menu Right', 'crb' ),
			'footer-menu' => __( 'Footer Menu', 'crb' ),
		) );		

		# Attach custom widgets
		include_once( CRB_THEME_DIR . 'options/widgets.php' );

		# Attach custom shortcodes
		include_once( CRB_THEME_DIR . 'options/shortcodes.php' );

		# Add Actions
		add_action( 'widgets_init', 'crb_widgets_init' );
		add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );

		# Add Image Size
		add_image_size( 'article-list-big', 550, 370, true );
		add_image_size( 'article-list-small-square', 300, 300, true );

		# Add Filters
		add_filter( 'excerpt_more', 'crb_excerpt_more' );
		add_filter( 'excerpt_length', 'crb_excerpt_length', 999 );
		add_filter( 'crb_theme_favicon_uri', function() {
			return get_template_directory_uri() . '/dist/images/favicon.ico';
		} );
		add_filter( 'carbon_fields_map_field_api_key', 'crb_get_google_maps_api_key' );
	}
}

# Register Sidebars
# Note: In a child theme with custom crb_setup_theme() this function is not hooked to widgets_init
function crb_widgets_init() {
	$sidebar_options = array_merge( crb_get_default_sidebar_options(), array(
		'name' => __( 'Default Sidebar', 'crb' ),
		'id'   => 'default-sidebar',
	) );

	register_sidebar( $sidebar_options );
}

# Sidebar Options
function crb_get_default_sidebar_options() {
	return array(
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widget__title">',
		'after_title'   => '</h2>',
	);
}

function crb_attach_theme_options() {
	# Attach fields
	include_once( CRB_THEME_DIR . 'options/theme-options.php' );
	include_once( CRB_THEME_DIR . 'options/post-meta.php' );
}

function crb_excerpt_more() {
	return '...';
}

function crb_excerpt_length() {
	return 55;
}

/**
 * Returns the Google Maps API Key set in Theme Options.
 *
 * @return string
 */
function crb_get_google_maps_api_key() {
	return carbon_get_theme_option( 'crb_google_maps_api_key' );
}

/**
 * Get the path to a versioned bundle relative to the theme directory.
 *
 * @param  string $path
 * @return string
 */
function crb_assets_bundle( $path ) {
	static $manifest = null;

	if ( is_null( $manifest ) ) {
		$manifest_path = CRB_THEME_DIR . 'dist/manifest.json';

		if ( file_exists( $manifest_path ) ) {
			$manifest = json_decode( file_get_contents( $manifest_path ), true );
		} else {
			$manifest = array();
		}
	}

	$path = isset( $manifest[ $path ] ) ? $manifest[ $path ] : $path;

	return '/dist/' . $path;
}

/**
 * Get the ID of the page using the given template name
 *
 * @param  string $template_name
 * @return string
 */
function crb_get_page_ID_by_template( $template_name ) {
	$pages = get_posts( array(
		'post_type' => 'page',
		'fields' => 'ids',
		'nopaging' => true,
		'meta_key' => '_wp_page_template',
		'meta_value' => $template_name,
	) );

	foreach ($pages as $page) {
		return $page;
	}
}

function crb_allow_svg_upload( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'crb_allow_svg_upload' );

add_action( 'admin_init', 'crb_hide_post_supports' );
function crb_hide_post_supports() {
	if ( isset( $_GET['post'] ) ) {
		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	}

	if ( ! empty( $post_id ) ) {
		$template_file = get_post_meta( $post_id, '_wp_page_template', true );

		switch ( $template_file ) {
			case 'templates/news.php':
				remove_post_type_support( 'page', 'editor' );
				break;

			case 'templates/for-investors.php':
				remove_post_type_support( 'page', 'editor' );
				break;

			case 'templates/investors-kit.php':
				remove_post_type_support( 'page', 'editor' );
				break;

			case 'templates/press-room.php':
				remove_post_type_support( 'page', 'editor' );
				break;

			case 'templates/in-the-news.php':
				remove_post_type_support( 'page', 'editor' );
				break;

			case 'templates/contact.php':
				remove_post_type_support( 'page', 'editor' );
				break;

			case 'templates/home.php':
				remove_post_type_support( 'page', 'editor' );
				remove_post_type_support( 'page', 'thumbnail' );
				break;

			case 'templates/our-story.php':
				remove_post_type_support( 'page', 'editor' );
				break;

			default:
				break;
		}
	}
}

add_action('pre_get_posts', 'query_post_type');
function query_post_type( $query ){
	if ( is_admin() ) {
		return;
	}

	if ( ! $query->is_main_query() ) {
		return;
	}

	if( ! $query->is_tax('crb_news_category') ){
		return;
	}

	$query->set('post_type', 'crb_news');
}

add_action('pre_get_posts', 'crb_pagination_offset');
function crb_pagination_offset( $query ){
	if ( is_admin() ) {
		return;
	}

	if ( ! $query->is_main_query() ) {
		return;
	}

	$page = $query->get('paged');
	if ( $page < 2 ) {
		$query->set('posts_per_page', 10);
	} else {
		$posts_per_page = get_option( 'posts_per_page', 5 );
		$offset = ( $page * $posts_per_page );
		$query->set( 'offset', $offset );
	}

}

add_filter( 'post_type_link', 'crb_post_type_link', 2, 10 );
add_filter( 'post_link', 'crb_post_type_link', 2, 10 );
function crb_post_type_link( $link, $post ) {
	if ( $post->post_type === 'crb_news' ) {
		if( $custom_link = get_field( 'link', $post->ID ) ) {
			$link = $custom_link;
		}
	}

	return $link;
}

//////////////////////////////////////////////////////////
//Remove tags from posts
//////////////////////////////////////////////////////////
function post_unregister_tags() {
	unregister_taxonomy_for_object_type('post_tag', 'post');
}
add_action('init', 'post_unregister_tags');