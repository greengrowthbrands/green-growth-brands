<?php
get_header();

crb_render_fragment( 'common/intro' );
?>

<div class="main main--gray">


		<div class="container">
			<article class="article-large">
				<div class="article__outer">
					<a href="<?php echo home_url(); ?>">
						<i class="fas fa-chevron-left"></i>

						<span><?php _e( 'back to home', 'crb' ); ?></span>
					</a>
				</div><!-- /.article__outer -->

				<div class="article__inner">
					<div class="article__content">
						<p>Please update your bookmarks as we’ve recently updated our website. The page you are looking for is no longer available.</p>
<p>To find the content you may be looking for, please try:</p>
<p>
Using the navigation bar above to continue browsing the site.<br>
Go back to the <a href="<?php echo site_url();?>">home page</a>.<br>
Or <a href="<?php echo site_url();?>/contact/">contact us</a>.</p>

					</div><!-- /.article__content -->

				</div><!-- /.article__inner -->
			</article><!-- /.article-large -->
		</div><!-- /.container -->

		<?php
		crb_render_fragment( 'blog/loop-single-related-stories' );

		crb_render_fragment( 'common/in-the-news-slider' );

		crb_render_fragment( 'common/subscribe' );

		crb_render_fragment( 'common/instafeed' );
		?>


</div><!-- /.main main-/-gray -->

<?php get_footer(); ?>
