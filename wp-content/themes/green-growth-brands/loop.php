<?php if ( have_posts() ) : ?>
	<section class="section section--category-archive section-articles-small">
		<div class="container">
			<div class="section__content">
				<div class="articles">
					<?php while ( have_posts() ) : the_post(); ?>
						<div class="article article--small article--col">
							<div class="article__image">
                                <img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'article-list-small-square' ); ?>" />
                            </div><!-- /.article__image -->
						
							<div class="article__head">
								<p class="article__meta"><?php echo get_the_date( 'F j, Y', get_the_ID() ); ?></p><!-- /.article__meta -->
						
								<h5 class="article__title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</h5><!-- /.article__title -->
							</div><!-- /.article__head -->
						
							<?php $terms = wp_get_post_terms( get_the_ID(), 'category' ); ?>

							<?php if ( $terms ) : $term = array_shift( $terms ); ?>
								<?php if(! is_category() ):?>
                                    <div class="article__actions">
                                        <a href="<?php echo get_term_link( $term ); ?>" class="btn-tag"><?php echo esc_html( $term->name ); ?></a>
                                    </div><!-- /.article__actions -->
                                <?php endif;?>
							<?php endif; ?>
						</div><!-- /.article -->
					<?php endwhile; ?>
				</div><!-- /.articles -->
			</div><!-- /.section__content -->

			<?php
			global $wp_query;
			$has_next_page = $wp_query->found_posts > 10 + ( $wp_query->query_vars['paged'] - 1 ) * get_option( 'posts_per_page', 5 );
			if ( $has_next_page ) :
				$next_page_link = next_posts( $wp_query->max_num_pages, false );
				?>
				<div class="section__actions">
					<?php if ( $next_page_link ) : ?>
						<a href="<?php echo $next_page_link; ?>" class="btn btn--primary btn--trasparent btn--large next-page-load"><?php _e( 'read more news', 'crb' ); ?></a>
					<?php endif; ?>
				</div><!-- /.section__actions -->
				<?php
			endif;
			?>
		</div><!-- /.container container-/-small -->
	</section><!-- /.section -->

	<?php
	crb_render_fragment( 'common/subscribe' );

	crb_render_fragment( 'common/instafeed' );
	?>
<?php else : ?>
	<section class="section">
		<div class="container container--large">
			<article class="article">
				<div class="row align-items-center justify-content-between">
					<div class="col-md-6">
						<p>
						<?php
						if ( is_category() ) { // If this is a category archive
							printf( __( "Sorry, but there aren't any posts in the %s category yet.", 'crb' ), single_cat_title( '', false ) );
						} else if ( is_date() ) { // If this is a date archive
							_e( "Sorry, but there aren't any posts with this date.", 'crb' );
						} else if ( is_author() ) { // If this is a category archive
							$userdata = get_user_by( 'id', get_queried_object_id() );
							printf( __( "Sorry, but there aren't any posts by %s yet.", 'crb' ), $userdata->display_name );
						} else if ( is_search() ) { // If this is a search
							_e( 'No posts found. Try a different search?', 'crb' );
						} else {
							_e( 'No posts found.', 'crb' );
						}
						?>
						</p>
					
						<?php get_search_form(); ?>
					</div><!-- /.col-md-6 -->
				</div><!-- /.row -->
			</article><!-- /.article -->
		</div><!-- /.container container-/-large -->
	</section><!-- /.section -->
<?php endif; ?>