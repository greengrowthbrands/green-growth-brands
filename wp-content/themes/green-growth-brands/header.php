<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

    <link rel="stylesheet" href="https://use.typekit.net/rtk8xmb.css">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="wrapper">
		<header class="header <?php echo is_front_page() ? 'header--dark' : '' ?> <?php echo is_404() ? 'header--404' : '' ?>">
			<div class="container">
				 <div class="header__inner">
					 <nav class="nav <?php echo is_front_page() ? '' : 'nav--white' ?>">
					 	<div class="nav__content">
						 	<a href="<?php echo home_url(); ?>" class="nav__logo visible-xs-inline-block visible-sm-inline-block">
						 		<img src="<?php echo get_template_directory_uri() ?>/resources/images/logo-mobile.svg" alt="">
						 	</a>

							
							<?php if ( has_nav_menu( 'main-menu-left' ) || has_nav_menu( 'main-menu-right' ) ) : ?>
								<ul>
							 		<?php
									if ( has_nav_menu( 'main-menu-left' ) ) {
										wp_nav_menu( array(
											'theme_location' => 'main-menu-left',
											'container' => false,
											'items_wrap' => '%3$s'
										) );
									}

							 		if ( has_nav_menu( 'main-menu-right' ) ) {
							 			echo str_replace( 'class="', 'class="visible-xs-inline-block visible-sm-inline-block ',
							 			wp_nav_menu( array(
							 				'theme_location' => 'main-menu-right',
							 				'container' => false,
							 				'items_wrap' => '%3$s',
							 				'depth' => 1,
											'echo' => false
							 			) )
							 		);
							 		}
						 			?>
							 	</ul>
							 <?php endif; ?>
					 	</div><!-- /.nav__content -->
					 </nav><!-- /.nav -->

					<?php if ( has_nav_menu( 'main-menu-left' ) || has_nav_menu( 'main-menu-right' ) ) : ?>
						<a href="#" class="nav-trigger">
							<span></span>
											 		
							<span></span>	
						</a>
					<?php endif; ?>
					 
					 <a href="<?php echo home_url(); ?>" class="logo">
					 	<?php if ( is_front_page() ) : ?>
						 	<img class="logo__image" src="<?php echo get_template_directory_uri() ?>/resources/images/logo-dark.svg" alt="">

						 	<img class="logo__image-fixed" src="<?php echo get_template_directory_uri() ?>/resources/images/logo.svg" alt="">
					 	<?php else :?>
					 		<img src="<?php echo get_template_directory_uri() ?>/resources/images/logo.svg" alt="">
					 	<?php endif; ?>
					 </a>
					 
					<?php
					if ( has_nav_menu( 'main-menu-right' ) ) {
						wp_nav_menu( array(
							'theme_location' => 'main-menu-right',
							'container' => 'nav',
							'container_class' => is_front_page() ? 'nav hidden-xs hidden-sm' : 'nav hidden-xs hidden-sm nav--white'
						) );
					}
					?>
				 </div><!-- /.header__inner -->
			</div><!-- /.container -->
		</header><!-- /.header -->