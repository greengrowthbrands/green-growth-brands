<?php
get_header();

crb_render_fragment( 'common/intro' );
?>

<div class="main main--gray">
	<?php while ( have_posts() ) : the_post(); ?>

		<div class="container">
			<article class="article-large">
				<div class="article__outer">
					<a href="<?php echo home_url(); ?>">
						<i class="fas fa-chevron-left"></i>
						
						<span><?php _e( 'back to home', 'crb' ); ?></span>
					</a>
				</div><!-- /.article__outer -->
					
				<div class="article__inner">
					<div class="article__content">
						<?php the_content(); ?>
					</div><!-- /.article__content -->

				</div><!-- /.article__inner -->
			</article><!-- /.article-large -->
		</div><!-- /.container -->

		<?php
		crb_render_fragment( 'blog/loop-single-related-stories' );

		crb_render_fragment( 'common/in-the-news-slider' );

		crb_render_fragment( 'common/subscribe' );

		crb_render_fragment( 'common/instafeed' );
		?>
	<?php endwhile; ?>

</div><!-- /.main main-/-gray -->

<?php get_footer(); ?>